const requestPromise = require('request-promise');
const dbUtil = require("../../config/dbUtil");
var bcrypt = require('bcryptjs');


exports.isVoid = function(obj)  {
	switch (typeof(obj)) {
		case "undefined":
		case "object":
			for (var x in obj) {
				if (obj.hasOwnProperty(x))
					return false;
				else
					return true;
			}
			return true;
		case "number":
		case "boolean":
			return false;
		case "string":
			if (obj.trim() === '')
				return true;
			else
				return false;
		default:
			return false;
	}
};

exports.getReqParams = function(params,src)  {
	var objToBeRet = {
		mandateData: {},
		optionalData: {},
		missing: []
	};

	params.mandateKeys && params.mandateKeys.forEach(function(param)  {
		exports.isVoid(src[param])
		? objToBeRet.missing.push(param)
		: objToBeRet.mandateData[param] = src[param];
	});
	params.optionalKeys && params.optionalKeys.forEach(function(param)  {
		if ( exports.isVoid(objToBeRet.mandateData[param]) )
			objToBeRet.optionalData[param] = exports.isVoid(src[param]) ? '' : src[param];
	});

	return objToBeRet;
};

exports.sendData = function(req, res, data)  {
	return res.status(200).json({
		success: true,
		data:data
	});
};

exports.sendWrongInputError = function(req, res, message, message_alias)  {
	return res.status(400).json({
		success: false,
		message:message,
		message_alias:message_alias
	});
};

exports.sendError = function(req, res, prettyMsg, err)  {
	return res.status(400).json({
		success: false,
		message: err.toString(),
		prettyMsg:prettyMsg,
		error: err.stack
	});
};

exports.sendEmail = function(to, from, subject, body)  {
	var mailBody = {
		from: from || 'donotreply@kryptosmobile.com',
		to: to,
		subject: subject,
		body: body
	};
	var urlOptions = {
		method: 'POST',
		url: "https://kryptos.kryptosmobile.com/gateway/CEAI/kryptosemailsender",
		headers: {
			'licenseKey': "UjpIaWVDVzpTOjE0NDk0ODk2MzUxMzE6VTpha0BjYW1wdXNlYWkub3JnOlQ6NDpQOjI4Ng=="
		},
		body: JSON.stringify(mailBody)
	}
	return requestPromise(urlOptions);	
};

exports.sendOTP = function(to, otp, tenantName)  {
	var urlOptions = {
		method: 'GET',
		url: "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&msg_type=TEXT&userid=2000173760&auth_scheme=plain&password=aboaDTHI1&v=1.1&format=text&send_to="+
			(to || '') + "&msg=" + (otp || '') + "%20is%20your%20One-Time%20Password%20(OTP)%20for%20" + (tenantName || '') + "."
	}
	requestPromise(urlOptions).then(function(result){}).catch(function(e){});	
};

exports.generateOTP = function()  {
	return Math.floor(Math.random() * 90000) + 10000;
};

exports.OTPMessageForMail = function(variables) {
	return (variables.otp || '') + ' is your One-Time Password (OTP) for ' + (variables.tenantName || '');
}

exports.sendOTPToMail = function(to, tenantName, phone)  {
	var otp = exports.generateOTP();
	return dbUtil.getConn()
		.then(function(db)  {
			var findQuery = {
				email: to
			};
			var updateQuery = {
				email: to,
				otp:otp
			}
			var queryOptions = {
				upsert: true
			};
			return db.collection(dbUtil.collection.mailOTP(tenantName)).update(findQuery, updateQuery, queryOptions);
		})
		.then(function(updateResult)  {
			var mailBody = exports.OTPMessageForMail({otp:otp, tenantName:tenantName});
			exports.sendOTP(phone, otp, tenantName);
			return exports.sendEmail(to, null, mailBody, mailBody);
		});
};

exports.customPromiseReject = function(prettyMsg, msg)  {
	return Promise.reject({
		isCustomMsg: true,
		prettyMsg: prettyMsg,
		message: msg
	});
}

exports.getPwdHash = function(pwd)  {
	return bcrypt.hash(pwd, 8);
};