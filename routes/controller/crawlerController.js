var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var extrequest = require('request');
var engine = require('../../core/engine');
var request = require('request');
var cheerio = require('cheerio');


exports.crawl = function(req, res, next) {
	/* check for mandatory request params */
	if(!engine.isValidRequest(req)) {		
		res.jsonp({"error":"missing mandatory parameter."});
		return;
	}

	var baseurl = engine.normalizeUrl(req);
	var tableName = "T_SYSTEM_CRAWLER_SESSIONS";
    var spidySession = {baseurl : baseurl, status : "In-Progress", 
    		linkscrawled : 0, queue : 0, social : {fb : [], tw : [], yt : [], li : [], gp : [], rss : [], images : []} };

	dbUtil.getConnection(function(db){
	    db.collection(tableName).insertOne(spidySession, function(err, result) {
			engine.triggerSpidy(baseurl, spidySession);
			res.json(spidySession);
	    });
	});
}

exports.crawlstatus = function(req, res, next) {
	if(req.body.key == null || req.body.key == undefined || req.body.key.length == 0){
		res.jsonp({"error":"missing mandatory parameter."});
		return;
	}
	dbUtil.getConnection(function(db){
		var tableName = "T_SYSTEM_CRAWLER_SESSIONS";
		var id = new ObjectId(req.body.key);
		db.collection(tableName).find({"_id": id}).toArray(function(err, result){
 			if (!result || result.length === 0) {
 				res.status(401).json({"Error":"No matching record found for crawler status"});
 			} else {
 				res.json(result[0]);
			}
		});
	});
}