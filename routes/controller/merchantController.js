var bcrypt = require('bcryptjs');
var geolib = require('geolib');
var sortBy = require('sort-by'),
    MerchantDetails = [];
var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var extrequest = require('request');
var sys = require('util');
var moment = require('moment-timezone');
var CoordinateTZ = require('coordinate-tz');
var zoneinfo = require('zoneinfo'),
    TZDate = zoneinfo.TZDate;


exports.create = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.Account && req.body.POC && req.body.restname && req.body.email) {
        var email = req.body.email;
        var phn = req.body.phn;
        var restname = req.body.restname;
        var restLogo = req.body.restLogo;
        var fax = req.body.fax;
        var lat = req.body.lat;
        var long = req.body.long;
        var restAdd = req.body.restAdd;
        var minTmDelivery = req.body.minTmDelivery;
        var minOrderAmt = req.body.minOrderAmt;
        var deliveryCharges = req.body.deliveryCharges;
        var discount = req.body.discount;
        var pickUpTime = req.body.pickUpTime;
        var deliveryAvailable = req.body.deliveryAvailable;
        var pickupAvailable = req.body.pickupAvailable;
        var POC = req.body.POC;
        var maxDistance = req.body.maxDistance;
        var Account = req.body.Account;
        var workingHrs = req.body.workingHrs;
        var couponDesc = req.body.couponDesc;
        var couponDiscount = req.body.couponDiscount;
        var rating = req.body.rating;
        var priceRange = req.body.priceRange;
        var tenant = req.body.tenant;
        var restCuisine = req.body.restCuisine;
        var merchantDiscount = req.body.merchantDiscount;
        var salesTax = req.body.salesTax;
        var appid = req.body.appid;
        var featureId = req.body.featureId;
        var restId = req.body.restId;
        var callOnPickUp = req.body.callOnPickUp;
        var callOnDelivery = req.body.callOnDelivery;
        var others = req.body.others;

        console.log(email);

        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_US_MERCHANTS";
                db.collection(tableName).find({
                    "email": email,
                    "tenant": tenant
                }).toArray(function (err, result) {
                    console.log(result);
                    if (result.length > 0) {
                        res.json({
                            "error": "Merchant email already exist."
                        });
                    } else {
                        var data = {
                            'email': email,
                            'phn': phn,
                            'restname': restname,
                            'fax': fax,
                            'lat': lat,
                            'long': long,
                            'restAdd': restAdd,
                            'restLogo': restLogo,
                            'minTmDelivery': minTmDelivery,
                            'minOrderAmt': minOrderAmt,
                            'deliveryCharges': deliveryCharges,
                            'discount': discount,
                            'pickUpTime': pickUpTime,
                            'deliveryAvailable': deliveryAvailable,
                            'pickupAvailable': pickupAvailable,
                            'POC': POC,
                            'maxDistance': maxDistance,
                            'Account': Account,
                            'workingHrs': workingHrs,
                            'couponDesc': couponDesc,
                            'couponDiscount': couponDiscount,
                            'rating': rating,
                            'priceRange': priceRange,
                            'tenant': tenant,
                            'restCuisine': restCuisine,
                            'merchantDiscount': merchantDiscount,
                            'salesTax': salesTax,
                            'appid': appid,
                            'featureId': featureId,
                            'restId': restId,
                            'callOnPickUp': callOnPickUp,
                            'callOnDelivery': callOnDelivery,
                            'username': restId,
                            'password': '12345',
                            'others': others
                        };
                        console.log('data added');
                        db.collection(tableName).insertOne(data, function (err, result3) {
                            res.json({
                                "success": "Merchant added successfully"
                            });
                        });
                    }
                });
            });
        } catch (e) {
            console.log(e)
        }
    } else {
        res.json({
            "Error": "Parameters missing"
        });
    }
}

exports.updateMerchantUS = function (req, res, next) {
    if (req.body && req.body._id && req.body.tenant && req.body.restname && req.body.email) {

        var email = req.body.email;
        var phn = req.body.phn;
        var restname = req.body.restname;
        var restLogo = req.body.restLogo;
        var fax = req.body.fax;
        var lat = req.body.lat;
        var long = req.body.long;
        var restAdd = req.body.restAdd;
        var minTmDelivery = req.body.minTmDelivery;
        var minOrderAmt = req.body.minOrderAmt;
        var deliveryCharges = req.body.deliveryCharges;
        var discount = req.body.discount;
        var pickUpTime = req.body.pickUpTime;
        var deliveryAvailable = req.body.deliveryAvailable;
        var pickupAvailable = req.body.pickupAvailable;
        var POC = req.body.POC;
        var maxDistance = req.body.maxDistance;
        var Account = req.body.Account;
        var workingHrs = req.body.workingHrs;
        var couponDesc = req.body.couponDesc;
        var couponDiscount = req.body.couponDiscount;
        var rating = req.body.rating;
        var priceRange = req.body.priceRange;
        var tenant = req.body.tenant;
        var restCuisine = req.body.restCuisine;
        var merchantDiscount = req.body.merchantDiscount;
        var salesTax = req.body.salesTax;
        var appid = req.body.appid;
        var featureId = req.body.featureId;
        var restId = req.body.restId;
        var callOnPickUp = req.body.callOnPickUp;
        var callOnDelivery = req.body.callOnDelivery;
        var username = req.body.username;
        var password = req.body.password;
        var others = req.body.others;


        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_US_MERCHANTS";
                db.collection(tableName).find({
                    "restId": restId
                }).toArray(function (err, result) {
                    console.log(result);
                    if (result.length == 0) {
                        res.json({
                            "error": "Merchant doesn't exist."
                        });
                    } else {
                        var id = new ObjectId(req.body._id);
                        var data = {
                            'email': email,
                            'phn': phn,
                            'restname': restname,
                            'fax': fax,
                            'lat': lat,
                            'long': long,
                            'restAdd': restAdd,
                            'restLogo': restLogo,
                            'minTmDelivery': minTmDelivery,
                            'minOrderAmt': minOrderAmt,
                            'deliveryCharges': deliveryCharges,
                            'discount': discount,
                            'pickUpTime': pickUpTime,
                            'deliveryAvailable': deliveryAvailable,
                            'pickupAvailable': pickupAvailable,
                            'POC': POC,
                            'maxDistance': maxDistance,
                            'Account': Account,
                            'workingHrs': workingHrs,
                            'couponDesc': couponDesc,
                            'couponDiscount': couponDiscount,
                            'rating': rating,
                            'priceRange': priceRange,
                            'tenant': tenant,
                            'restCuisine': restCuisine,
                            'merchantDiscount': merchantDiscount,
                            'salesTax': salesTax,
                            'appid': appid,
                            'featureId': featureId,
                            'restId': restId,
                            'callOnPickUp': callOnPickUp,
                            'callOnDelivery': callOnDelivery,
                            'username': username,
                            'password': password,
                            'others': others
                        };
                        db.collection(tableName).updateOne({"_id": id}, {$set: data}, function (err, result3) {
                            res.json({
                                "success": "Merchant updated successfully"
                            });
                        });
                    }
                });
            });
        } catch (e) {
            console.log(e)
        }
    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }
}

exports.getMerchants = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.lat && req.body.long) {
        var tenant = req.body.tenant;
        var getMinutes = function (str) {
            var time = str.split(':');
            return time[0] * 60 + time[1] * 1;
        }
        var getMinutesNow = function (str) {
            var timeNow = new Date();
            return timeNow.getHours() * 60 + timeNow.getMinutes();
        }
        var checkRestaurantTiming = function (tm, now) {
            var found = false;
            var times = tm.split("#");
            for (var k = 0; k < times.length; k++) {
                var start = getMinutes(times[k].split(" - ")[0]);
                var end = getMinutes(times[k].split(" - ")[1]);

                if ((now >= start) && (now <= end)) {
                    found = true;
                    break;
                } else {
                    found = false;
                }
            }
            return found;
        }
        dbUtil.getConnection(function (db) {
            var tableName = "T_" + tenant + "_MERCHANTS";
            db.collection(tableName).find({}).toArray(function (err, result) {
                var MerchantDetails = [];
                var openRestaurant = [];
                var closedRestaurant = [];
                if (result.length === 0) {
                    res.json({
                        "error": "No Merchant Found."
                    });
                } else {
                    var day = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

                    for (var i = 0; i < result.length; i++) {
                        try {

                            if (result[i].email) {
                                var merchantLat = result[i].lat;
                                var merchantLong = result[i].long;
                                console.log(merchantLat + " " + merchantLong);

                                query = CoordinateTZ.calculate(merchantLat, merchantLong);
                                var d = new TZDate();
                                d.setTimezone(query.timezone);
                                var d = d.toString();
                                var today = new Date();
                                var date = moment.tz(today, query.timezone).format();

                                var yyyy = date.substring(0, date.indexOf("-"));
                                var mm = date.substring(date.indexOf("-") + 1, date.indexOf("-") + 3);
                                var dd = date.substring(date.indexOf("-") + 4, date.indexOf("T"));
                                var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
                                var minutes = date.substr(date.indexOf(":") + 1, 2);
                                var currentDate = new Date(yyyy, mm - 1, dd, hours, minutes, 0, 0);


                                var CurrentDay = day[currentDate.getDay()];
                                var crtm = hours + ":" + minutes;
                                var now = getMinutes(crtm);

                                switch (CurrentDay) {
                                    case 'Mon': {
                                        var tm = result[i].workingHrs.monday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        var openHour = result[i].openHours.Mon;
                                        break;
                                    }
                                    case 'Tue': {
                                        var tm = result[i].workingHrs.tuesday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        var openHour = result[i].openHours.Tue;
                                        break;
                                    }
                                    case 'Wed': {
                                        var tm = result[i].workingHrs.wednesday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        var openHour = result[i].openHours.Wed;
                                        break;
                                    }
                                    case 'Thu': {
                                        var tm = result[i].workingHrs.thursday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        var openHour = result[i].openHours.Thus;
                                        break;
                                    }
                                    case 'Fri': {
                                        var tm = result[i].workingHrs.friday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        var openHour = result[i].openHours.Fri;
                                        break;
                                    }
                                    case 'Sat': {
                                        var tm = result[i].workingHrs.saturday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        var openHour = result[i].openHours.Sat;
                                        break;
                                    }
                                    case 'Sun': {
                                        var tm = result[i].workingHrs.sunday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        var openHour = result[i].openHours.Sun;
                                        break;
                                    }
                                }
                                if (RestaurantIsOpen == true) {
                                    try {
                                        var distance = geolib.getDistance({
                                            latitude: req.body.lat,
                                            longitude: req.body.long
                                        }, {
                                            latitude: result[i].lat,
                                            longitude: result[i].long
                                        });
                                    } catch (e) {
                                    }
                                    result[i].distance = distance;
                                    result[i].open = "Yes";
                                    result[i].openHour = openHour;
                                    openRestaurant.push(result[i]);
                                } else {
                                    var distance = geolib.getDistance({
                                        latitude: req.body.lat,
                                        longitude: req.body.long
                                    }, {
                                        latitude: result[i].lat,
                                        longitude: result[i].long
                                    });
                                    result[i].distance = distance;
                                    result[i].open = "No";
                                    result[i].openHour = openHour;
                                    closedRestaurant.push(result[i]);
                                }

                            } else {
                                //result.splice(i, 1);
                            }
                        } catch (e) {
                            console.log(e)
                        }
                    }
                    MerchantDetails = openRestaurant;
                    MerchantDetails.sort(sortBy('distance'));
                    res.json({"openRestaurant": MerchantDetails, "closedRestaurant": closedRestaurant});
                }
            });
        });
    } else {
        res.status(401).json({
            "Error": "Body should contain tenant, lat, long"
        });
    }
}
exports.getMerchantsUS = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.lat && req.body.long) {
        var tenant = req.body.tenant;
        var getMinutes = function (str) {
            var time = str.split(':');
            return time[0] * 60 + time[1] * 1;
        }
        var getMinutesNow = function (str) {
            var timeNow = new Date();
            return timeNow.getHours() * 60 + timeNow.getMinutes();
        }
        var checkRestaurantTiming = function (tm, now) {
            var found = false;
            var times = tm.split("#");
            for (var k = 0; k < times.length; k++) {
                var start = getMinutes(times[k].split(" - ")[0]);
                var end = getMinutes(times[k].split(" - ")[1]);

                if ((now >= start) && (now <= end)) {
                    found = true;
                    break;
                } else {
                    found = false;
                }
            }
            return found;
        }
        dbUtil.getConnection(function (db) {
            var tableName = "T_BIGDEAL_US_MERCHANTS";
            db.collection(tableName).find({
                "tenant": tenant
            }).toArray(function (err, result) {
                var MerchantDetails = [];
                var openRestaurant = [];
                var closedRestaurant = [];
                if (result.length === 0) {
                    res.json({
                        "error": "No Merchant Found."
                    });
                } else {
                    var day = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

                    for (var i = 0; i < result.length; i++) {
                        try {

                            if (result[i].email) {
                                var merchantLat = result[i].lat;
                                var merchantLong = result[i].long;
                                console.log(merchantLat + " " + merchantLong);

                                query = CoordinateTZ.calculate(merchantLat, merchantLong);
                                console.log(query);
                                var d = new TZDate();
                                d.setTimezone(query.timezone);
                                var d = d.toString();
                                var today = new Date();
                                var date = moment.tz(today, query.timezone).format();

                                console.log(date);

                                var yyyy = date.substring(0, date.indexOf("-"));
                                var mm = date.substring(date.indexOf("-") + 1, date.indexOf("-") + 3);
                                var dd = date.substring(date.indexOf("-") + 4, date.indexOf("T"));
                                var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
                                var minutes = date.substr(date.indexOf(":") + 1, 2);
                                var currentDate = new Date(yyyy, mm - 1, dd, hours, minutes, 0, 0);

                                console.log(currentDate);


                                var CurrentDay = day[currentDate.getDay()];
                                var crtm = hours + ":" + minutes;
                                var now = getMinutes(crtm);
                                var openHour = "";

                                switch (CurrentDay) {
                                    case 'Mon': {
                                        var tm = result[i].workingHrs.monday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Mon;
                                        break;
                                    }
                                    case 'Tue': {
                                        var tm = result[i].workingHrs.tuesday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Tue;
                                        break;
                                    }
                                    case 'Wed': {
                                        var tm = result[i].workingHrs.wednesday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Wed;
                                        break;
                                    }
                                    case 'Thu': {
                                        var tm = result[i].workingHrs.thursday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Thus;
                                        break;
                                    }
                                    case 'Fri': {
                                        var tm = result[i].workingHrs.friday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Fri;
                                        break;
                                    }
                                    case 'Sat': {
                                        var tm = result[i].workingHrs.saturday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Sat;
                                        break;
                                    }
                                    case 'Sun': {
                                        var tm = result[i].workingHrs.sunday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Sun;
                                        break;
                                    }
                                }
                                console.log(openHour);

                                var distance = geolib.getDistance({
                                    latitude: req.body.lat,
                                    longitude: req.body.long
                                }, {
                                    latitude: result[i].lat,
                                    longitude: result[i].long
                                });

                                var miles = geolib.convertUnit('mi', distance, 2);

                                console.log(miles + "\n" + result[i].maxDistance);

                                if (miles <= result[i].maxDistance) {
                                    if (RestaurantIsOpen == true) {
                                        result[i].distance = distance;
                                        result[i].open = "Yes";
                                        result[i].openHour = openHour;
                                        openRestaurant.push(result[i]);
                                    } else {

                                        result[i].distance = distance;
                                        result[i].open = "No";
                                        result[i].openHour = openHour;
                                        closedRestaurant.push(result[i]);
                                    }

                                }


                            } else {
                                //result.splice(i, 1);
                            }
                        } catch (e) {
                            console.log(e)
                        }
                    }
                    MerchantDetails = openRestaurant;
                    MerchantDetails.sort(sortBy('distance'));
                    res.json({"openRestaurant": MerchantDetails, "closedRestaurant": closedRestaurant});
                }
            });
        });
    } else {
        res.status(401).json({
            "Error": "Body should contain tenant, lat, long"
        });
    }
}
exports.getAllMerchantsUS = function (req, res, next) {

    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_BIGDEAL_US_MERCHANTS";
            db.collection(tableName).find(
                {},
                {
                    restname: 1,
                    restId: 1,
                    password: 1,
                    _id: 0
                })
                .toArray(function (err, result) {
                    res.json(result.sort(sortBy('restId')));
                });
        });

    } catch (e) {

        console.log(e);

    }

}


exports.addUserOrder = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.order && req.body.emailID && req.body.restId) {
        var tenant = req.body.tenant;
        var order = req.body.order;
        var emailID = req.body.emailID;
        var restId = req.body.restId;

        var d = new Date();
        var month = d.getMonth() + 1;
        var date = d.getDate() + "/" + month + "/" + d.getFullYear();
        try {
            var orderId = "ORDER" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1) + "" + Math.floor((Math.random() * 10) + 1);
            console.log(orderId);
            dbUtil.getConnection(function (db) {
                var tableName = "T_" + tenant + "_USERORDERS";
                db.collection(tableName).find({
                    "orderID": orderId,
                    "emailID": emailID
                }).toArray(function (err, result) {
                    console.log(result);
                    if (result.length > 0) {
                        res.json({
                            "error": "Order ID and email ID already exists."
                        });
                    } else {
                        var data = {
                            'orderID': orderId,
                            'emailID': emailID,
                            'order': order,
                            'tenant': tenant,
                            'date': date,
                            'restId': restId
                        };
                        db.collection(tableName).insertOne(data, function (err, result) {
                            res.json({
                                "success": "order added", "orderInfo": orderId
                            });
                        });
                    }
                });
            });
        } catch (e) {
            console.log(e)
        }
    } else {
        res.status(401).json({
            "Error": "Body should contain tenant, orderID, order, emailID, restId"
        });
    }
}

exports.addUserOrderUS = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.order && req.body.emailID && req.body.restId && req.body.name && req.body.phone && req.body.address && req.body.paymentMode && req.body.orderId && req.body.restName && req.body.restPhone && req.body.restAdd) {
        var order = req.body.order;
        var emailID = req.body.emailID;
        var restId = req.body.restId;
        var tenant = req.body.tenant;
        var name = req.body.name;
        var phone = req.body.phone;
        var address = req.body.address;
        var paymentMode = req.body.paymentMode;
        var orderId = req.body.orderId;
        var restName = req.body.restName;
        var restAdd = req.body.restAdd;
        var restPhone = req.body.restPhone;
        var lat = req.body.lat;
        var long = req.body.long;
        var customer = req.body.customer;

        query = CoordinateTZ.calculate(lat, long);
        var today = new Date();
        var date = moment.tz(today, query.timezone).format();
        /* date will a sting like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.indexOf("-") + 3);
        var dd = date.substring(date.indexOf("-") + 4, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);
        var currentDate = new Date(yyyy, mm - 1, dd, hours, minutes, 0, 0);

        today = yyyy + '-' + mm + '-' + dd;

        var data = {
            'orderID': orderId,
            'emailID': emailID,
            'name': name,
            'phone': phone,
            'address': address,
            'order': order,
            'tenant': tenant,
            'date': today,
            'restId': restId,
            'paymentMode': paymentMode,
            "restName": restName,
            "restAdd": restAdd,
            "restPhone": restPhone,
            "customer": customer,
            'Timestamp': currentDate.getTime(),
            'status': "pending"
        };
        if (req.body.ready)
            data.ready = req.body.ready;
        if (req.body.delivered)
            data.delivered = req.body.delivered;


        try {
            console.log(orderId);
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_US_USERORDERS";
                db.collection(tableName).find({
                    "orderID": orderId,
                    "emailID": emailID
                }).toArray(function (err, result) {
                    if (err) {
                        return res.json({
                            "success": false, "message": "Unable to process order."
                        });
                    }
                    if (result.length > 0) {
                        res.json({
                            "success": false, "message": "Order already exists."
                        });
                    } else {
                        db.collection(tableName).insertOne(data, function (err, result) {
                            res.json({
                                "success": true, "orderInfo": orderId
                            });
                        });
                    }
                });
            });
        } catch (e) {
            console.log(e)
        }
    } else {
        res.status(401).json({
            "Error": "Missing parameters."
        });
    }
}

exports.getBigdealMerchant = function (req, res) {

    var id = req.body.id;
    var source = req.body.source;

    if (!id)
        return res.send('Missing id.');

    if (!source)
        return res.send('Missing source.');

    var find = {};

    if (source == 'QRCode')
        find['restId'] = id;
    else
        find['phn'] = id;


    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_BIGDEAL_MERCHANTS";
            db.collection(tableName).find(find).toArray(function (err, result) {
                if (err) {
                    res.json({success: false, message: "Something went wrong. Please try again."});

                }
                if (result.length > 0) {
                    res.json({success: true, merchantDetail: result[0]});
                } else {
                    res.json({success: false, message: "Merchant doesn't exist."});
                }
            });
        });
    } catch (e) {
        console.log(e)
    }

}


exports.getUserOrders = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.emailID) {
        var tenant = req.body.tenant;
        var emailID = req.body.emailID;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_" + tenant + "_USERORDERS";
                db.collection(tableName).find({
                    "emailID": emailID
                }).toArray(function (err, result) {
                    if (result.length > 0) {
                        res.json(result);
                    } else {
                        res.json({"error": "order not found."});
                    }
                });
            });
        } catch (e) {
            console.log(e)
        }
    } else {
        res.status(401).json({
            "Error": "Body should contain tenant, emailID"
        });
    }
}

exports.getUserOrdersUS = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.emailID) {
        var tenant = req.body.tenant;
        var emailID = req.body.emailID;
        if (req.body.date) {
            var date = req.body.date;
            if (!validateDate(date))
                return res.send("Invalid date, date must be of format YYYY-MM-DD.");
            try {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_BIGDEAL_US_USERORDERS";
                    db.collection(tableName).find({
                        "emailID": emailID,
                        "date": date
                    }).toArray(function (err, result) {
                        if (result.length > 0) {
                            var orders = [];
                            for (var i = 0; i < result.length; i++) {
                                var orderObject = result[i];
                                var pushData = {
                                    "orderID": orderObject.orderID,
                                    "emailID": orderObject.emailID,
                                    "name": orderObject.name,
                                    "payableamount": orderObject.order.payableamount,
                                    "orderType": orderObject.order.orderType,
                                    "status": orderObject.status,
                                    "Timestamp": orderObject.Timestamp
                                };
                                orders.push(pushData);
                            }
                            res.json({"success": true, "result": orders});
                        } else {
                            res.json({"success": false, "message": "No order for today."});
                        }
                    });
                });
            } catch (e) {
                console.log(e)
            }

        } else {
            try {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_BIGDEAL_US_USERORDERS";
                    db.collection(tableName).find({
                        "emailID": emailID
                    }).toArray(function (err, result) {
                        if (result.length > 0) {
                            var orders = [];
                            for (var i = 0; i < result.length; i++) {
                                var orderObject = result[i];
                                var pushData = {
                                    "orderID": orderObject.orderID,
                                    "emailID": orderObject.emailID,
                                    "name": orderObject.name,
                                    "payableamount": orderObject.order.payableamount,
                                    "orderType": orderObject.order.orderType,
                                    "status": orderObject.status,
                                    "Timestamp": orderObject.Timestamp
                                };
                                orders.push(pushData);
                            }
                            res.json({"success": true, "result": orders});
                        } else {
                            res.json({"success": false, "message": "You haven't placed any order."});
                        }
                    });
                });
            } catch (e) {
                console.log(e)
            }

        }
    } else {
        res.status(401).json({
            "Error": "Body should contain tenant, emailID"
        });
    }
}


exports.getTenantOrders = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.date) {
        var tenant = req.body.tenant;
        var date = req.body.date;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_" + tenant + "_USERORDERS";
                db.collection(tableName).find({
                    "date": date
                }).toArray(function (err, result) {
                    if (result.length > 0) {
                        res.json(result);
                    } else {
                        res.json({"error": "order not found."});
                    }
                });
            });
        } catch (e) {
            console.log(e)
        }
    } else {
        var tenant = req.body.tenant;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_" + tenant + "_USERORDERS";
                db.collection(tableName).find({}).toArray(function (err, result) {
                    if (result.length > 0) {
                        res.json(result);
                    } else {
                        res.json({"error": "order not found."});
                    }
                });
            });
        } catch (e) {
            console.log(e)
        }
    }
}


exports.getMerchantOrders = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.restId) {
        var tenant = req.body.tenant;
        var restId = req.body.restId;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_" + tenant + "_USERORDERS";
                db.collection(tableName).find({
                    "restId": restId
                }).toArray(function (err, result) {
                    if (result.length > 0) {
                        res.json(result);
                    } else {
                        res.json({"error": "order not found."});
                    }
                });
            });
        } catch (e) {
            console.log(e)
        }
    } else {
        res.json({"error": "parameters missing tenant, restaurant ID."});
    }
}

exports.getMerchantOrdersUS = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.restId) {
        var tenant = req.body.tenant;
        var restId = req.body.restId;

        if (req.body.date) {
            var date = req.body.date;
            try {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_BIGDEAL_US_USERORDERS";
                    db.collection(tableName).find({
                        "restId": restId,
                        "date": date
                    }).toArray(function (err, result) {
                        if (result.length > 0) {
                            var orders = [];
                            for (var i = 0; i < result.length; i++) {
                                var orderObject = result[i];
                                var pushData = {
                                    "orderID": orderObject.orderID,
                                    "emailID": orderObject.emailID,
                                    "name": orderObject.name,
                                    "payableamount": orderObject.order.payableamount,
                                    "orderType": orderObject.order.orderType,
                                    "status": orderObject.status,
                                    "Timestamp": orderObject.Timestamp
                                };
                                orders.push(pushData);
                            }
                            res.json({"success": true, "result": orders});
                        } else {
                            res.json({"success": false, "message": "No order for today."});
                        }
                    });
                });
            } catch (e) {
                console.log(e)
            }

        } else {
            try {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_BIGDEAL_US_USERORDERS";
                    db.collection(tableName).find({
                        "restId": restId
                    }).toArray(function (err, result) {
                        if (result.length > 0) {
                            var orders = [];
                            for (var i = 0; i < result.length; i++) {
                                var orderObject = result[i];
                                var pushData = {
                                    "orderID": orderObject.orderID,
                                    "emailID": orderObject.emailID,
                                    "name": orderObject.name,
                                    "payableamount": orderObject.order.payableamount,
                                    "orderType": orderObject.order.orderType,
                                    "status": orderObject.status,
                                    "Timestamp": orderObject.Timestamp
                                };
                                orders.push(pushData);
                            }
                            res.json({"success": true, "result": orders});
                        } else {
                            res.json({"success": false, "message": "No order for today."});
                        }
                    });
                });
            } catch (e) {
                console.log(e)
            }

        }
    } else {
        res.json({"error": "parameters missing tenant, restaurant ID."});
    }
}

exports.merchantLoginUS = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.username && req.body.password) {
        var username = req.body.username;
        var password = req.body.password;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_US_MERCHANTS";
                db.collection(tableName).find({
                    'username': username
                }).toArray(function (err, result) {
                    if (result.length > 0) {
                        var merchantDetail = result[0];
                        if (merchantDetail.password == password) {
                            res.json({'success': true, 'merchantDetails': result});
                        } else {
                            res.json({"success": false, "message": "Invalid username and password."});
                        }
                    } else {
                        res.json({"success": false, "message": "Invalid username and password."});
                    }
                });
            });
        } catch (e) {
            console.log(e)
        }


    } else {
        res.json({"error": "parameters missing username,password"});
    }
}

exports.getBigDealOrderStatusUS = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.orderId) {
        var orderId = req.body.orderId;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_US_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderId
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.json({"success": false, "message": "Order doesn't exist."});
                    } else {
                        var data = result[0];
                        res.json({"success": true, "status": data.status});
                    }
                });
            });
        } catch (e) {
        }
    } else {
        res.json({"error": "missing parameters."});
    }
}

exports.getBigDealOrderStatusUS = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.orderId) {
        var orderId = req.body.orderId;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_US_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderId
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.json({"success": false, "message": "Order doesn't exist."});
                    } else {
                        var data = result[0];
                        res.json({"success": true, "status": data.status});

                    }
                });
            });

        } catch (e) {
        }


    } else {
        res.json({"error": "missing parameters."});
    }

}
exports.getBigDealOrdersDetailUS = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.orderID && req.body.emailID) {
        var orderId = req.body.orderID;
        var emailID = req.body.emailID;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_US_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderId,
                    'emailID': emailID
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.json({"success": false, "message": "Order doesn't exist."});
                    } else {
                        var data = result[0];
                        res.json({"success": true, "order": data});
                    }
                });
            });

        } catch (e) {
        }


    } else {
        res.json({"error": "missing parameters."});
    }

}

exports.getBigDealOrdersUS = function (req, res, next) {
    if (req.body && req.body.to && req.body.from) {
        var to = req.body.to;
        var from = req.body.from;


        var getYear = function (date) {
            return date.substring(0, date.indexOf("-"));
        }
        var getMonth = function (date) {
            return date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        }
        var getDay = function (date) {
            return date.substring(date.lastIndexOf("-") + 1, date.length);
        }

        /* validate "to" date */
        if (!validateDate(to))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");

        /* validate "from" date */
        if (!validateDate(from))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");


        var toDate = new Date(getYear(to), getMonth(to) - 1, getDay(to), 23, 59, 59);
        toDate = toDate.getTime();

        var fromDate = new Date(getYear(from), getMonth(from) - 1, getDay(from), 0, 0, 0);
        fromDate = fromDate.getTime();

        if (fromDate > toDate) {
            res.send("Start date must be less than End date.")
        } else {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_US_USERORDERS";
                db.collection(tableName).find({
                    "Timestamp": {
                        $gte: fromDate,
                        $lte: toDate
                    }
                }).toArray(function (err, result) {
                    if (err) {
                        res.json({"error": "Something went wrong."});
                    } else {
                        res.json(result);
                    }
                });
            });
        }
    } else {
        res.json({"error": "Missing parameters"});
    }
}

exports.merchantOrderResponseUS = function (req, res, next) {

    if (req.body && req.body.tenant && req.body.emailID && req.body.orderID && req.body.status) {

        var emailID = req.body.emailID;
        var orderID = req.body.orderID;
        var order_status = req.body.status;
        var orderDetail = req.body.orderDetail;
        var previousOrder = req.body.previousOrder;
        var declinedBy = req.body.declinedBy;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_US_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderID,
                    'emailID': emailID
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.json({'success': false, 'message': "Order doesn't exists."});
                    } else {
                        var data = result[0];
                        if (order_status == 'confirmed') {
                            data.status = "confirmed";
                            if (previousOrder) {
                                data.previousOrder = data.order;
                                data.order = orderDetail;
                            }
                            data.customer = undefined;
                        }
                        else if (order_status == 'declined') {
                            data.status = "declined";
                            if (declinedBy && declinedBy == 'M') {
                                data.customer = undefined;
                            }
                        }

                        else if (order_status == 'pending')
                            data.status = 'pending';

                        db.collection(tableName).updateOne({'_id': data._id}, {$set: data}, function (err, result) {
                            if (err) {
                                res.json({'success': false, 'message': "Something went wrong."});
                            } else {
                                if (order_status == 'confirmed')
                                    res.json({'success': true, 'message': "Order has been successfully confirmed."});
                                else if (order_status == 'declined')
                                    res.json({'success': true, 'message': "Order has been declined."});
                                else if (order_status == 'pending')
                                    res.json({'success': true, 'message': "Order is in pending state."});
                            }
                        });

                    }
                });
            });

        } catch (e) {
        }
    }
    else {
        res.json({'success': false, 'message': "Missing paramters."});
    }

}

exports.merchantOrderStatusUpdateUS = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.orderID && req.body.emailID) {

        var orderId = req.body.orderID;
        var emailID = req.body.emailID;
        var ready = "";
        var delivered = "";
        if (req.body.ready)
            ready = req.body.ready;
        if (req.body.delivered)
            delivered = req.body.delivered;
        try {

            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_US_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderId,
                    'emailID': emailID
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.json({"success": false, "message": "Order doesn't exist."});
                    } else {
                        var data = result[0];
                        if (ready && ready == 'Yes') {
                            data.ready = ready;
                        } else if (delivered && delivered == 'Yes') {
                            data.delivered = delivered;
                        }
                        db.collection(tableName).updateOne({'_id': data._id}, {$set: data}, function (err, result) {
                            if (err) {
                                res.json({'success': false, 'message': "Something went wrong."});
                            } else {
                                if (ready)
                                    res.json({'success': true, 'message': "Order is ready."});
                                else
                                    res.json({'success': true, 'message': "Order id delivered."});
                            }
                        });

                    }
                });
            });

        } catch (e) {
        }


    } else {
        res.json({"error": "missing parameters."});
    }

}

exports.updateMerchantToken = function (req, res, next) {

    var restId = req.body.restId;
    var type = req.body.type;
    var token = req.body.token;

    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_BIGDEAL_US_MERCHANTS";
            db.collection(tableName).find({
                'restId': restId
            }).toArray(function (err, result) {
                if (result.length > 0) {
                    var merchantDetail = result[0];
                    merchantDetail.type = type;
                    merchantDetail.token = token;
                    db.collection(tableName).updateOne({'_id': merchantDetail._id}, {$set: merchantDetail}, function (err, result) {
                        if (err) {
                            res.json({'success': false, 'message': "Something went wrong."});
                        } else {
                            res.json({'success': true, 'message': "Token saved."});
                        }
                    });
                } else {
                    res.json({"success": false, "message": "Merchant doesnt exists."});
                }
            });
        });
    } catch (e) {
        console.log(e)
    }

}

exports.autoDeclineOrder = function (req, res) {

    query = CoordinateTZ.calculate(36.1529747, -86.7925971);
    var today = new Date();
    var date = moment.tz(today, query.timezone).format();
    /* date will a sting like-  2016-07-08T18:09:18+05:30   */

    /* get year,month,day,hours,minutes from date string */
    var yyyy = date.substring(0, date.indexOf("-"));
    var mm = date.substring(date.indexOf("-") + 1, date.indexOf("-") + 3);
    var dd = date.substring(date.indexOf("-") + 4, date.indexOf("T"));
    var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
    var minutes = date.substr(date.indexOf(":") + 1, 2);
    var currentDate = new Date(yyyy, mm - 1, dd, hours, minutes, 0, 0);

    const intervalTimestamp = 3 * 600000;

    var timestampNow = currentDate.getTime();

    var timestampIntervalbefore = timestampNow - intervalTimestamp;

    try {
        dbUtil.getConnection(function (db) {
            var tableName = "T_BIGDEAL_US_USERORDERS";
            db.collection(tableName).find({
                status: "pending",
                "Timestamp": {
                    $lte: timestampIntervalbefore
                }
            }).toArray(function (err, result) {


                if (result.length > 0) {
                    declineAllOrders(0, result, db, function (err, data) {
                        res.json({"success": true, "message": result.length + " orders declined."});
                    })
                } else {
                    res.json({"success": true, "message": "No orders found."});
                }
            });
        });
    } catch (e) {
        console.log(e)
    }


}
function declineAllOrders(index, result, db, cb) {
    var tableName = "T_BIGDEAL_US_USERORDERS";

    if (index < result.length) {

        var order = result[index];
        order.status = "declined";
        order.customer = undefined;

        db.collection(tableName).updateOne({'_id': order._id}, {$set: order}, function (err, data) {
            sendEmail(order.orderID, order.emailID, order.tenant, order.order.restSelect.email, function (err, data) {
                index++;
                declineAllOrders(index, result, db, cb);
            });
        });


    } else {
        cb(null, true);
    }
}


function sendEmail(orderId, to, tenant, restEmail, callback) {
    // alert("sending mail");

    console.log(restEmail);


    var emailBody = "<html><body>";
    emailBody += "<div style='width:100%;color: #000;text-align: left;vertical-align: 30px'><b>Your order (" + orderId + ") was declined due to merchant unavailability.</b></div><br/>";
    emailBody += "<div style='width:100%;color: #000;text-align: left;vertical-align: 30px'><b>For any queries or clarification, please call us on (844) 620-0002 (Toll Free).</b></div><br/>";
    emailBody += "<div style='width:100%;color: #000;text-align: left;vertical-align: 30px'><b>Thank you,</b></div><br/>";
    emailBody += "<div style='width:100%;color: #000;text-align: left;vertical-align: 30px'><b>" + tenant + " app</b></div><br/>";
    emailBody += "</body></html>";


    var emailData = {
        'to': to + ";" + restEmail + ";vendortransaction@kryptosmobile.com",
        //'to': "vendortransaction@kryptosmobile.com;bigdeals@campuseai.org",
        //'to': 'aashish@getbigdeal.co',
        'subject': orderId + " Declined",
        'body': emailBody,
        'from': 'order@kryptosmobile.com',
        'attachment': true

    };
    var emailOptions = {
        url: "https://kryptos.kryptosmobile.com/gateway/CEAI/kryptosemailsender",
        headers: {
            'licenseKey': "UjpIaWVDVzpTOjE0NDk0ODk2MzUxMzE6VTpha0BjYW1wdXNlYWkub3JnOlQ6NDpQOjI4Ng=="
        },
        body: JSON.stringify(emailData)
    }
    extrequest.post(emailOptions, function (err, eresp, ebody) {
        /*console.log("Email resp : " + ebody);
         console.log("Email err : " + err);
         console.log("Email eresp : " + JSo);*/
        callback(null, true);
    });

}


/******************Production Services for Merchant API in India************/

exports.addBigDealUserOrder = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.order && req.body.emailID && req.body.restId && req.body.name && req.body.phone && req.body.address && req.body.paymentMode && req.body.orderId && req.body.restName && req.body.restPhone && req.body.restAdd) {
        var order = req.body.order;
        var emailID = req.body.emailID;
        var restId = req.body.restId;
        var tenant = req.body.tenant;
        var name = req.body.name;
        var phone = req.body.phone;
        var address = req.body.address;
        var paymentMode = req.body.paymentMode;
        var orderId = req.body.orderId;
        var restName = req.body.restName;
        var restAdd = req.body.restAdd;
        var restPhone = req.body.restPhone;


        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a sting like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);


        var currentDate = new Date(yyyy, mm - 1, dd, hours, minutes, 0, 0);
        today = yyyy + '-' + mm + '-' + dd;

        var data = {
            'orderID': orderId,
            'emailID': emailID,
            'name': name,
            'phone': phone,
            'address': address,
            'order': order,
            'tenant': tenant,
            'date': today,
            'restId': restId,
            'paymentMode': paymentMode,
            "restName": restName,
            "restAdd": restAdd,
            "restPhone": restPhone,
            'Timestamp': currentDate.getTime(),
            'status': "pending"
        };
        if (req.body.ready)
            data.ready = req.body.ready;
        if (req.body.delivered)
            data.delivered = req.body.delivered;


        try {
            console.log(orderId);
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_USERORDERS";
                db.collection(tableName).find({
                    "orderID": orderId,
                    "emailID": emailID
                }).toArray(function (err, result) {
                    console.log(result);
                    if (result.length > 0) {
                        res.json({
                            "success": false, "message": "Order already exists."
                        });
                    } else {
                        db.collection(tableName).insertOne(data, function (err, result) {
                            res.json({
                                "success": true, "orderInfo": orderId
                            });
                        });
                    }
                });
            });
        } catch (e) {
            console.log(e)
        }
    } else {
        res.status(401).json({
            "Error": "Missing parameters."
        });
    }
}


exports.getBigDealUserOrders = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.emailID) {
        var tenant = req.body.tenant;
        var emailID = req.body.emailID;
        if (req.body.date) {
            var date = req.body.date;
            if (!validateDate(date))
                return res.send("Invalid date, date must be of format YYYY-MM-DD.");

            try {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_BIGDEAL_USERORDERS";
                    db.collection(tableName).find({
                        "emailID": emailID,
                        "date": date
                    }).toArray(function (err, result) {
                        if (result.length > 0) {
                            var orders = [];
                            for (var i = 0; i < result.length; i++) {
                                var orderObject = result[i];
                                var pushData = {
                                    "orderID": orderObject.orderID,
                                    "emailID": orderObject.emailID,
                                    "name": orderObject.name,
                                    "payableamount": orderObject.order.payableamount,
                                    "orderType": orderObject.order.orderType,
                                    "status": orderObject.status,
                                    "Timestamp": orderObject.Timestamp
                                };
                                orders.push(pushData);
                            }
                            res.json({"success": true, "result": orders});
                        } else {
                            res.json({"success": false, "message": "No order for today."});
                        }
                    });
                });
            } catch (e) {
                console.log(e)
            }

        } else {
            try {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_BIGDEAL_USERORDERS";
                    db.collection(tableName).find({
                        "emailID": emailID
                    }).toArray(function (err, result) {
                        if (result.length > 0) {
                            var orders = [];
                            for (var i = 0; i < result.length; i++) {
                                var orderObject = result[i];
                                var pushData = {
                                    "orderID": orderObject.orderID,
                                    "emailID": orderObject.emailID,
                                    "name": orderObject.name,
                                    "payableamount": orderObject.order.payableamount,
                                    "orderType": orderObject.order.orderType,
                                    "status": orderObject.status,
                                    "Timestamp": orderObject.Timestamp
                                };
                                orders.push(pushData);
                            }
                            res.json({"success": true, "result": orders});
                        } else {
                            res.json({"success": false, "message": "No order for today."});
                        }
                    });
                });
            } catch (e) {
                console.log(e)
            }

        }
    } else {
        res.status(401).json({
            "Error": "Body should contain tenant, emailID"
        });
    }
}

exports.getBigDealMerchantOrders = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.restId) {
        var tenant = req.body.tenant;
        var restId = req.body.restId;

        if (req.body.date) {
            var date = req.body.date;
            try {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_BIGDEAL_USERORDERS";
                    db.collection(tableName).find({
                        "restId": restId,
                        "date": date
                    }).toArray(function (err, result) {
                        if (result.length > 0) {
                            var orders = [];
                            for (var i = 0; i < result.length; i++) {
                                var orderObject = result[i];
                                var pushData = {
                                    "orderID": orderObject.orderID,
                                    "emailID": orderObject.emailID,
                                    "name": orderObject.name,
                                    "payableamount": orderObject.order.payableamount,
                                    "orderType": orderObject.order.orderType,
                                    "status": orderObject.status,
                                    "Timestamp": orderObject.Timestamp
                                };
                                orders.push(pushData);
                            }
                            res.json({"success": true, "result": orders});
                        } else {
                            res.json({"success": false, "message": "No order for today."});
                        }
                    });
                });
            } catch (e) {
                console.log(e)
            }

        } else {
            try {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_BIGDEAL_USERORDERS";
                    db.collection(tableName).find({
                        "restId": restId
                    }).toArray(function (err, result) {
                        if (result.length > 0) {
                            var orders = [];
                            for (var i = 0; i < result.length; i++) {
                                var orderObject = result[i];
                                var pushData = {
                                    "orderID": orderObject.orderID,
                                    "emailID": orderObject.emailID,
                                    "name": orderObject.name,
                                    "payableamount": orderObject.order.payableamount,
                                    "orderType": orderObject.order.orderType,
                                    "status": orderObject.status,
                                    "Timestamp": orderObject.Timestamp
                                };
                                orders.push(pushData);
                            }
                            res.json({"success": true, "result": orders});
                        } else {
                            res.json({"success": false, "message": "No order for today."});
                        }
                    });
                });
            } catch (e) {
                console.log(e)
            }

        }
    } else {
        res.json({"error": "parameters missing tenant, restaurant ID."});
    }
}

exports.createBigDealMerchant = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.Account && req.body.POC && req.body.restname && req.body.email) {

        var email = req.body.email;
        var phn = req.body.phn;
        var restname = req.body.restname;
        var restLogo = req.body.restLogo;
        var fax = req.body.fax;
        var lat = req.body.lat;
        var long = req.body.long;
        var restAdd = req.body.restAdd;
        var minTmDelivery = req.body.minTmDelivery;
        var minOrderAmt = req.body.minOrderAmt;
        var deliveryCharges = req.body.deliveryCharges;
        var discount = req.body.discount;
        var pickUpTime = req.body.pickUpTime;
        var deliveryAvailable = req.body.deliveryAvailable;
        var pickupAvailable = req.body.pickupAvailable;
        var POC = req.body.POC;
        var maxDistance = req.body.maxDistance;
        var Account = req.body.Account;
        var workingHrs = req.body.workingHrs;
        var couponDesc = req.body.couponDesc;
        var couponDiscount = req.body.couponDiscount;
        var rating = req.body.rating;
        var priceRange = req.body.priceRange;
        var tenant = req.body.tenant;
        var restCuisine = req.body.restCuisine;
        var vat = req.body.vat;
        var serviceCharge = req.body.serviceCharge;
        var serviceTax = req.body.serviceTax;
        var merchantDiscount = req.body.merchantDiscount;
        var appid = req.body.appid;
        var featureId = req.body.featureId;
        var restId = req.body.restId;
        var callOnPickUp = req.body.callOnPickUp;
        var callOnDelivery = req.body.callOnDelivery;
        var notificationOnPickUp = req.body.notificationOnPickUp;
        var notificationOnDelivery = req.body.notificationOnDelivery;
        var smsOnPickUp = req.body.smsOnPickUp;
        var smsOnDelivery = req.body.smsOnDelivery;
        var paytmOnPickUp = req.body.paytmOnPickUp;
        var paytmOnDelivery = req.body.paytmOnDelivery;
        var codOnPickUp = req.body.codOnPickUp;
        var codOnDelivery = req.body.codOnDelivery;
        var supportReadyAlert = req.body.supportReadyAlert;
        var supportDeliveryAlert = req.body.supportDeliveryAlert;
        var others = req.body.others;

        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_MERCHANTS";
                db.collection(tableName).find({
                    "email": email,
                    "restId": restId
                }).toArray(function (err, result) {
                    console.log(result);
                    if (result.length > 0) {
                        res.json({
                            "success": false, "message": "Merchant already exist."
                        });
                    } else {
                        var data = {
                            'email': email,
                            'phn': phn,
                            'restname': restname,
                            'fax': fax,
                            'lat': lat,
                            'long': long,
                            'restAdd': restAdd,
                            'restLogo': restLogo,
                            'minTmDelivery': minTmDelivery,
                            'minOrderAmt': minOrderAmt,
                            'deliveryCharges': deliveryCharges,
                            'discount': discount,
                            'pickUpTime': pickUpTime,
                            'deliveryAvailable': deliveryAvailable,
                            'pickupAvailable': pickupAvailable,
                            'POC': POC,
                            'maxDistance': maxDistance,
                            'Account': Account,
                            'workingHrs': workingHrs,
                            'couponDesc': couponDesc,
                            'couponDiscount': couponDiscount,
                            'rating': rating,
                            'priceRange': priceRange,
                            'tenant': tenant,
                            'restCuisine': restCuisine,
                            'vat': vat,
                            'serviceCharge': serviceCharge,
                            'serviceTax': serviceTax,
                            'merchantDiscount': merchantDiscount,
                            'appid': appid,
                            'featureId': featureId,
                            'restId': restId,
                            'others': others,
                            'callOnPickUp': callOnPickUp,
                            'callOnDelivery': callOnDelivery,
                            'notificationOnPickUp': notificationOnPickUp,
                            'notificationOnDelivery': notificationOnDelivery,
                            'smsOnPickUp': smsOnPickUp,
                            'smsOnDelivery': smsOnDelivery,
                            'paytmOnPickUp': paytmOnPickUp,
                            'paytmOnDelivery': paytmOnDelivery,
                            'codOnPickUp': codOnPickUp,
                            'codOnDelivery': codOnDelivery,
                            'supportReadyAlert': supportReadyAlert,
                            'supportDeliveryAlert': supportDeliveryAlert,
                            'username': restId,
                            'password': "12345"
                        };
                        console.log('data added');
                        db.collection(tableName).insertOne(data, function (err, result3) {
                            if (err) {
                                res.json({
                                    "success": false, "message": "Failed to add merchant."
                                });
                            } else {
                                res.json({
                                    "success": true, "message": "Merchant added successfully."
                                });
                            }
                        });
                    }
                });
            });
        } catch (e) {
            console.log(e)
            res.json({"error": e});
        }
    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }
}
exports.updateBigDealMerchant = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.Account && req.body.POC && req.body.restname && req.body.email) {

        var email = req.body.email;
        var phn = req.body.phn;
        var restname = req.body.restname;
        var restLogo = req.body.restLogo;
        var fax = req.body.fax;
        var lat = req.body.lat;
        var long = req.body.long;
        var restAdd = req.body.restAdd;
        var minTmDelivery = req.body.minTmDelivery;
        var minOrderAmt = req.body.minOrderAmt;
        var deliveryCharges = req.body.deliveryCharges;
        var discount = req.body.discount;
        var pickUpTime = req.body.pickUpTime;
        var deliveryAvailable = req.body.deliveryAvailable;
        var pickupAvailable = req.body.pickupAvailable;
        var POC = req.body.POC;
        var maxDistance = req.body.maxDistance;
        var Account = req.body.Account;
        var workingHrs = req.body.workingHrs;
        var couponDesc = req.body.couponDesc;
        var couponDiscount = req.body.couponDiscount;
        var rating = req.body.rating;
        var priceRange = req.body.priceRange;
        var tenant = req.body.tenant;
        var restCuisine = req.body.restCuisine;
        var vat = req.body.vat;
        var serviceCharge = req.body.serviceCharge;
        var serviceTax = req.body.serviceTax;
        var merchantDiscount = req.body.merchantDiscount;
        var appid = req.body.appid;
        var featureId = req.body.featureId;
        var restId = req.body.restId;
        var callOnPickUp = req.body.callOnPickUp;
        var callOnDelivery = req.body.callOnDelivery;
        var notificationOnPickUp = req.body.notificationOnPickUp;
        var notificationOnDelivery = req.body.notificationOnDelivery;
        var smsOnPickUp = req.body.smsOnPickUp;
        var smsOnDelivery = req.body.smsOnDelivery;
        var paytmOnPickUp = req.body.paytmOnPickUp;
        var paytmOnDelivery = req.body.paytmOnDelivery;
        var codOnPickUp = req.body.codOnPickUp;
        var codOnDelivery = req.body.codOnDelivery;
        var supportReadyAlert = req.body.supportReadyAlert;
        var supportDeliveryAlert = req.body.supportDeliveryAlert;
        var username = req.body.username;
        var password = req.body.password;
        var others = req.body.others;

        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_MERCHANTS";
                db.collection(tableName).find({
                    "email": email,
                    "restId": restId
                }).toArray(function (err, result) {
                    if (result.length > 0) {
                        var data = result[0];
                        data.email = email;
                        data.phn = phn;
                        data.restname = restname;
                        data.fax = fax;
                        data.lat = lat;
                        data.long = long;
                        data.restAdd = restAdd;
                        data.restLogo = restLogo;
                        data.minTmDelivery = minTmDelivery;
                        data.minOrderAmt = minOrderAmt;
                        data.deliveryCharges = deliveryCharges;
                        data.discount = discount;
                        data.pickUpTime = pickUpTime;
                        data.deliveryAvailable = deliveryAvailable;
                        data.pickupAvailable = pickupAvailable;
                        data.POC = POC;
                        data.maxDistance = maxDistance;
                        data.Account = Account;
                        data.workingHrs = workingHrs;
                        data.couponDesc = couponDesc;
                        data.couponDiscount = couponDiscount;
                        data.rating = rating;
                        data.priceRange = priceRange;
                        data.tenant = tenant;
                        data.restCuisine = restCuisine;
                        data.vat = vat;
                        data.serviceCharge = serviceCharge;
                        data.serviceTax = serviceTax;
                        data.merchantDiscount = merchantDiscount;
                        data.appid = appid;
                        data.featureId = featureId;
                        data.restId = restId;
                        data.others = others;
                        data.callOnPickUp = callOnPickUp;
                        data.callOnDelivery = callOnDelivery;
                        data.notificationOnPickUp = notificationOnPickUp;
                        data.notificationOnDelivery = notificationOnDelivery;
                        data.smsOnPickUp = smsOnPickUp;
                        data.smsOnDelivery = smsOnDelivery;
                        data.paytmOnPickUp = paytmOnPickUp;
                        data.paytmOnDelivery = paytmOnDelivery;
                        data.codOnPickUp = codOnPickUp;
                        data.codOnDelivery = codOnDelivery;
                        data.supportReadyAlert = supportReadyAlert;
                        data.supportDeliveryAlert = supportDeliveryAlert;
                        data.username = username;
                        data.password = password;
                        db.collection(tableName).updateOne({'_id': data._id}, {$set: data}, function (err, result) {
                            if (err) {
                                res.json({"success": false, "message": "Failed to update merchant."})
                            } else {
                                res.json({"success": true, "message": "Merchant updated successfully."})
                            }
                        });

                    } else {
                        res.json({
                            "success": false, "message": "Merchant doesn't exist."
                        });
                    }
                });
            });
        } catch (e) {
            console.log(e)
            res.json({"error": e});
        }
    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }
}


exports.merchantLogin = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.username && req.body.password) {
        var username = req.body.username;
        var password = req.body.password;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_MERCHANTS";
                db.collection(tableName).find({
                    'username': username
                }).toArray(function (err, result) {
                    if (result.length > 0) {
                        var merchantDetail = result[0];
                        if (merchantDetail.password == password) {
                            res.json({'success': true, 'merchantDetails': result});
                        } else {
                            res.json({"success": false, "message": "Invalid username and password."});
                        }
                    } else {
                        res.json({"success": false, "message": "Invalid username and password."});
                    }
                });
            });
        } catch (e) {
            console.log(e)
        }


    } else {
        res.json({"error": "parameters missing username,password"});
    }
}

exports.getBigDealOrderStatus = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.orderId) {
        var orderId = req.body.orderId;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderId
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.json({"success": false, "message": "Order doesn't exist."});
                    } else {
                        var data = result[0];
                        res.json({"success": true, "status": data.status});

                    }
                });
            });

        } catch (e) {
        }


    } else {
        res.json({"error": "missing parameters."});
    }

}
exports.getBigDealOrdersDetail = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.orderID && req.body.emailID) {
        var orderId = req.body.orderID;
        var emailID = req.body.emailID;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderId,
                    'emailID': emailID
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.json({"success": false, "message": "Order doesn't exist."});
                    } else {
                        var data = result[0];
                        res.json({"success": true, "order": data});
                    }
                });
            });

        } catch (e) {
        }


    } else {
        res.json({"error": "missing parameters."});
    }

}

exports.calcDelivery = function (req, res) {
    if (req.body && req.body.lat && req.body.long && req.body.merclat && req.body.merclong && req.body.variableDelivery) {
        var lat = req.body.lat;
        var long = req.body.long;

        var merclat = req.body.merclat;
        var merclong = req.body.merclong;
        var variableDelivery = req.body.variableDelivery;

        var distance = geolib.getDistance(
            {latitude: lat, longitude: long},
            {latitude: merclat, longitude: merclong}
        );
        /* convert to miles*/
        distance = distance * 0.000621371;
        console.log(distance);
        for (var i = 0; i < variableDelivery.length; i++) {

            if (distance <= variableDelivery[i].dist) {
                return res.json({cost: variableDelivery[i].cost});
            }
            if (i == (variableDelivery.length - 1) && distance > variableDelivery[i].dist) {
                return res.json({cost: variableDelivery[i].cost});
            }
        }

    } else {
        res.json({error: 'missing parameters.'});
    }
}


exports.getBigDealOrders = function (req, res, next) {
    if (req.body && req.body.to && req.body.from) {
        var to = req.body.to;
        var from = req.body.from;


        var getYear = function (date) {
            return date.substring(0, date.indexOf("-"));
        }
        var getMonth = function (date) {
            return date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        }
        var getDay = function (date) {
            return date.substring(date.lastIndexOf("-") + 1, date.length);
        }

        /* validate "to" date */
        if (!validateDate(to))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");

        /* validate "from" date */
        if (!validateDate(from))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");


        var toDate = new Date(getYear(to), getMonth(to) - 1, getDay(to), 23, 59, 59);
        toDate = toDate.getTime();

        var fromDate = new Date(getYear(from), getMonth(from) - 1, getDay(from), 0, 0, 0);
        fromDate = fromDate.getTime();

        if (fromDate > toDate) {
            res.send("Start date must be less than End date.")
        } else {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_USERORDERS";
                db.collection(tableName).find({
                    "Timestamp": {
                        $gte: fromDate,
                        $lte: toDate
                    }
                }).toArray(function (err, result) {
                    if (err) {
                        res.json({"error": "Something went wrong."});
                    } else {
                        res.json(result);
                    }
                });
            });

        }


    } else {
        res.json({"error": "Missing parameters"});
    }

}


exports.getBigDealMerchants = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.lat && req.body.long) {

        var tenant = req.body.tenant;
        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a string like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);
        var currentDate = new Date(yyyy, mm - 1, dd, hours, minutes, 0, 0);
        var tenant = req.body.tenant;
        var getMinutes = function (str) {
            var time = str.split(':');
            return time[0] * 60 + time[1] * 1;
        }
        var getMinutesNow = function (str) {

            return currentDate.getHours() * 60 + currentDate.getMinutes();
        }
        var checkRestaurantTiming = function (tm, now) {
            var found = false;
            try {
                var times = tm.split("#");
                for (var k = 0; k < times.length; k++) {
                    var start = getMinutes(times[k].split(" - ")[0]);
                    var end = getMinutes(times[k].split(" - ")[1]);

                    if ((now >= start) && (now <= end)) {
                        found = true;
                        break;
                    } else {
                        found = false;
                    }
                }
            } catch (e) {
            }
            return found;
        }
        dbUtil.getConnection(function (db) {
            var tableName = "T_BIGDEAL_MERCHANTS";
            db.collection(tableName).find({
                'tenant': tenant
            }).toArray(function (err, result) {
                var MerchantDetails = [];
                var openRestaurant = [];
                var closedRestaurant = [];
                if (result.length === 0) {
                    res.json({
                        "success": false, "message": "No merchant exists."
                    });
                } else {
                    var day = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

                    for (var i = 0; i < result.length; i++) {
                        try {

                            if (result[i].email) {

                                var CurrentDay = day[currentDate.getDay()];
                                var crtm = currentDate.getHours() + ":" + currentDate.getMinutes();
                                var now = getMinutes(crtm);
                                var openHour = "";
                                switch (CurrentDay) {
                                    case 'Mon': {
                                        var tm = result[i].workingHrs.monday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Mon;
                                        break;
                                    }
                                    case 'Tue': {
                                        var tm = result[i].workingHrs.tuesday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Tue;
                                        break;
                                    }
                                    case 'Wed': {
                                        var tm = result[i].workingHrs.wednesday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Wed;
                                        break;
                                    }
                                    case 'Thu': {
                                        var tm = result[i].workingHrs.thursday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Thus;
                                        break;
                                    }
                                    case 'Fri': {
                                        var tm = result[i].workingHrs.friday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Fri;
                                        break;
                                    }
                                    case 'Sat': {
                                        var tm = result[i].workingHrs.saturday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Sat;
                                        break;
                                    }
                                    case 'Sun': {
                                        var tm = result[i].workingHrs.sunday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        if (result[i].others.openHours)
                                            openHour = result[i].others.openHours.Sun;
                                        break;
                                    }
                                }
                                console.log(openHour);
                                if (RestaurantIsOpen == true) {
                                    try {
                                        var distance = geolib.getDistance({
                                            latitude: req.body.lat,
                                            longitude: req.body.long
                                        }, {
                                            latitude: result[i].lat,
                                            longitude: result[i].long
                                        });
                                    } catch (e) {
                                    }
                                    result[i].distance = distance;
                                    result[i].open = "Yes";
                                    result[i].openHour = openHour;
                                    openRestaurant.push(result[i]);
                                } else {
                                    var distance = geolib.getDistance({
                                        latitude: req.body.lat,
                                        longitude: req.body.long
                                    }, {
                                        latitude: result[i].lat,
                                        longitude: result[i].long
                                    });
                                    result[i].distance = distance;
                                    result[i].open = "No";
                                    result[i].openHour = openHour;
                                    closedRestaurant.push(result[i]);
                                }

                            } else {
                                //result.splice(i, 1);
                            }
                        } catch (e) {
                            console.log(e)
                        }
                    }
                    MerchantDetails = openRestaurant;
                    MerchantDetails.sort(sortBy('distance'));
                    res.json({
                        "success": true,
                        "openRestaurant": MerchantDetails,
                        "closedRestaurant": closedRestaurant
                    });
                }
            });
        });
    } else {
        res.status(401).json({
            "Error": "Body should contain tenant, lat, long"
        });
    }
}
exports.getBigDealCanteenMerchants = function (req, res, next) {
    if (req.body && req.body.tenant) {

        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a string like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);
        var currentDate = new Date(yyyy, mm - 1, dd, hours, minutes, 0, 0);
        var tenant = req.body.tenant;
        var getMinutes = function (str) {
            var time = str.split(':');
            return time[0] * 60 + time[1] * 1;
        }
        var getMinutesNow = function (str) {

            return currentDate.getHours() * 60 + currentDate.getMinutes();
        }
        var checkRestaurantTiming = function (tm, now) {
            var found = false;
            try {
                var times = tm.split("#");
                for (var k = 0; k < times.length; k++) {
                    var start = getMinutes(times[k].split(" - ")[0]);
                    var end = getMinutes(times[k].split(" - ")[1]);

                    if ((now >= start) && (now <= end)) {
                        found = true;
                        break;
                    } else {
                        found = false;
                    }
                }
            } catch (e) {
            }
            return found;
        }
        dbUtil.getConnection(function (db) {
            var tableName = "T_BIGDEAL_MERCHANTS";
            db.collection(tableName).find({
                'tenant': tenant,
                'restId': {$regex: /.*canteen.*/i}
            }).toArray(function (err, result) {
                var MerchantDetails = [];
                var openRestaurant = [];
                var closedRestaurant = [];
                if (result.length === 0) {
                    res.json({
                        "success": false, "message": "No merchant exists."
                    });
                } else {
                    var day = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

                    for (var i = 0; i < result.length; i++) {
                        try {

                            if (result[i].email) {

                                var CurrentDay = day[currentDate.getDay()];
                                var crtm = currentDate.getHours() + ":" + currentDate.getMinutes();
                                var now = getMinutes(crtm);
                                switch (CurrentDay) {
                                    case 'Mon': {
                                        var tm = result[i].workingHrs.monday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                    case 'Tue': {
                                        var tm = result[i].workingHrs.tuesday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                    case 'Wed': {
                                        var tm = result[i].workingHrs.wednesday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                    case 'Thu': {
                                        var tm = result[i].workingHrs.thursday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                    case 'Fri': {
                                        var tm = result[i].workingHrs.friday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                    case 'Sat': {
                                        var tm = result[i].workingHrs.saturday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                    case 'Sun': {
                                        var tm = result[i].workingHrs.sunday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                }
                                if (RestaurantIsOpen == true) {
                                    try {
                                        var distance = geolib.getDistance({
                                            latitude: req.body.lat,
                                            longitude: req.body.long
                                        }, {
                                            latitude: result[i].lat,
                                            longitude: result[i].long
                                        });
                                    } catch (e) {
                                    }
                                    result[i].distance = distance;
                                    result[i].open = "Yes";
                                    openRestaurant.push(result[i]);
                                } else {
                                    /*var distance = geolib.getDistance({
                                     latitude: req.body.lat,
                                     longitude: req.body.long
                                     }, {
                                     latitude: result[i].lat,
                                     longitude: result[i].long
                                     });
                                     result[i].distance = distance;*/
                                    result[i].open = "No";
                                    closedRestaurant.push(result[i]);
                                }

                            } else {
                                //result.splice(i, 1);
                            }
                        } catch (e) {
                            console.log(e)
                        }
                    }
                    MerchantDetails = openRestaurant;
                    MerchantDetails.sort(sortBy('distance'));
                    res.json({
                        "success": true,
                        "openRestaurant": MerchantDetails,
                        "closedRestaurant": closedRestaurant
                    });
                }
            });
        });

    } else {
        res.json({"success": false, "message": "Missing parameters."});
    }
}
exports.merchantOrderResponse = function (req, res, next) {

    if (req.body && req.body.tenant && req.body.emailID && req.body.orderID && req.body.status) {

        var emailID = req.body.emailID;
        var orderID = req.body.orderID;
        var order_status = req.body.status;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderID,
                    'emailID': emailID
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.json({'success': false, 'message': "Order doesn't exists."});
                    } else {
                        var data = result[0];
                        if (order_status == 'confirmed')
                            data.status = "confirmed";
                        else if (order_status == 'declined')
                            data.status = "declined";
                        db.collection(tableName).updateOne({'_id': data._id}, {$set: data}, function (err, result) {
                            if (err) {
                                res.json({'success': false, 'message': "Something went wrong."});
                            } else {
                                if (order_status == 'confirmed')
                                    res.json({'success': true, 'message': "Order has been successfully confirmed."});
                                else
                                    res.json({'success': true, 'message': "Order has been declined."});
                            }
                        });

                    }
                });
            });

        } catch (e) {
        }
    }
    else {
        res.json({'success': false, 'message': "Missing paramters."});

    }

}

exports.merchantOrderStatusUpdate = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.orderID && req.body.emailID) {
        var orderId = req.body.orderID;
        var emailID = req.body.emailID;

        var ready = "";
        var delivered = "";
        if (req.body.ready)
            ready = req.body.ready;
        if (req.body.delivered)
            delivered = req.body.delivered;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderId,
                    'emailID': emailID
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.json({"success": false, "message": "Order doesn't exist."});
                    } else {
                        var data = result[0];
                        if (ready && ready == 'Yes') {
                            data.ready = ready;
                        } else if (delivered && delivered == 'Yes') {
                            data.delivered = delivered;
                        }
                        db.collection(tableName).updateOne({'_id': data._id}, {$set: data}, function (err, result) {
                            if (err) {
                                res.json({'success': false, 'message': "Something went wrong."});
                            } else {
                                if (ready)
                                    res.json({'success': true, 'message': "Order is ready."});
                                else
                                    res.json({'success': true, 'message': "Order id delivered."});
                            }
                        });


                    }
                });
            });

        } catch (e) {
        }


    } else {
        res.json({"error": "missing parameters."});
    }

}


/****************QA services for Merchant API*************/

exports.addBigDealUserOrderTest = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.order && req.body.emailID && req.body.restId && req.body.name && req.body.phone && req.body.address && req.body.paymentMode && req.body.orderId && req.body.restName && req.body.restPhone && req.body.restAdd) {
        var order = req.body.order;
        var emailID = req.body.emailID;
        var restId = req.body.restId;
        var tenant = req.body.tenant;
        var name = req.body.name;
        var phone = req.body.phone;
        var address = req.body.address;
        var paymentMode = req.body.paymentMode;
        var orderId = req.body.orderId;
        var restName = req.body.restName;
        var restAdd = req.body.restAdd;
        var restPhone = req.body.restPhone;


        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a sting like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);


        var currentDate = new Date(yyyy, mm - 1, dd, hours, minutes, 0, 0);
        today = yyyy + '-' + mm + '-' + dd;
        var data = {
            'orderID': orderId,
            'emailID': emailID,
            'name': name,
            'phone': phone,
            'address': address,
            'order': order,
            'tenant': tenant,
            'date': today,
            'restId': restId,
            'paymentMode': paymentMode,
            "restName": restName,
            "restAdd": restAdd,
            "restPhone": restPhone,
            'Timestamp': currentDate.getTime(),
            'status': "pending"
        };
        if (req.body.ready)
            data.ready = req.body.ready;
        if (req.body.delivered)
            data.delivered = req.body.delivered;

        try {
            console.log(orderId);
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_USERORDERS";
                db.collection(tableName).find({
                    "orderID": orderId,
                    "emailID": emailID
                }).toArray(function (err, result) {

                    if (result.length > 0) {
                        res.json({
                            "success": false, "message": "Order already exists."
                        });
                    } else {
                        db.collection(tableName).insertOne(data, function (err, result) {
                            res.json({
                                "success": true, "orderInfo": orderId
                            });
                        });
                    }
                });
            });
        } catch (e) {
            console.log(e)
        }
    } else {
        res.status(401).json({
            "Error": "Missing parameters."
        });
    }
}

exports.getBigDealUserOrdersTest = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.emailID) {
        var tenant = req.body.tenant;
        var emailID = req.body.emailID;
        if (req.body.date) {
            var date = req.body.date;

            try {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_DUMMY_BIGDEAL_USERORDERS";
                    db.collection(tableName).find({
                        "emailID": emailID,
                        "date": date
                    }).toArray(function (err, result) {
                        if (result.length > 0) {
                            var orders = [];
                            for (var i = 0; i < result.length; i++) {
                                var orderObject = result[i];
                                var pushData = {
                                    "orderID": orderObject.orderID,
                                    "emailID": orderObject.emailID,
                                    "name": orderObject.name,
                                    "payableamount": orderObject.order.payableamount,
                                    "orderType": orderObject.order.orderType,
                                    "status": orderObject.status,
                                    "Timestamp": orderObject.Timestamp
                                };
                                orders.push(pushData);
                            }
                            res.json({"success": true, "result": orders});
                        } else {
                            res.json({"success": false, "message": "No order for today."});
                        }
                    });
                });
            } catch (e) {
                console.log(e)
            }

        } else {
            try {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_DUMMY_BIGDEAL_USERORDERS";
                    db.collection(tableName).find({
                        "emailID": emailID
                    }).toArray(function (err, result) {
                        if (result.length > 0) {
                            var orders = [];
                            for (var i = 0; i < result.length; i++) {
                                var orderObject = result[i];
                                var pushData = {
                                    "orderID": orderObject.orderID,
                                    "emailID": orderObject.emailID,
                                    "name": orderObject.name,
                                    "payableamount": orderObject.order.payableamount,
                                    "orderType": orderObject.order.orderType,
                                    "status": orderObject.status,
                                    "Timestamp": orderObject.Timestamp
                                };
                                orders.push(pushData);
                            }
                            res.json({"success": true, "result": orders});
                        } else {
                            res.json({"success": false, "message": "You haven't placed any order."});
                        }
                    });
                });
            } catch (e) {
                console.log(e)
            }

        }
    } else {
        res.status(401).json({
            "Error": "Body should contain tenant, emailID"
        });
    }
}

exports.getBigDealMerchantOrdersTest = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.restId) {
        var tenant = req.body.tenant;
        var restId = req.body.restId;

        if (req.body.date) {
            var date = req.body.date;
            try {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_DUMMY_BIGDEAL_USERORDERS";
                    db.collection(tableName).find({
                        "restId": restId,
                        "date": date
                    }).toArray(function (err, result) {
                        if (result.length > 0) {
                            var orders = [];
                            for (var i = 0; i < result.length; i++) {
                                var orderObject = result[i];
                                var pushData = {
                                    "orderID": orderObject.orderID,
                                    "emailID": orderObject.emailID,
                                    "name": orderObject.name,
                                    "payableamount": orderObject.order.payableamount,
                                    "orderType": orderObject.order.orderType,
                                    "status": orderObject.status,
                                    "Timestamp": orderObject.Timestamp
                                };
                                orders.push(pushData);
                            }
                            res.json({"success": true, "result": orders});
                        } else {
                            res.json({"success": false, "message": "No order for today."});
                        }
                    });
                });
            } catch (e) {
                console.log(e)
            }

        } else {
            try {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_DUMMY_BIGDEAL_USERORDERS";
                    db.collection(tableName).find({
                        "restId": restId
                    }).toArray(function (err, result) {
                        if (result.length > 0) {
                            var orders = [];
                            for (var i = 0; i < result.length; i++) {
                                var orderObject = result[i];
                                var pushData = {
                                    "orderID": orderObject.orderID,
                                    "emailID": orderObject.emailID,
                                    "name": orderObject.name,
                                    "payableamount": orderObject.order.payableamount,
                                    "orderType": orderObject.order.orderType,
                                    "status": orderObject.status,
                                    "Timestamp": orderObject.Timestamp
                                };
                                orders.push(pushData);
                            }
                            res.json({"success": true, "result": orders});
                        } else {
                            res.json({"success": false, "message": "No order for today."});
                        }
                    });
                });
            } catch (e) {
                console.log(e)
            }

        }
    } else {
        res.json({"error": "parameters missing tenant, restaurant ID."});
    }
}
exports.getBigDealOrdersTest = function (req, res, next) {
    if (req.body && req.body.to && req.body.from) {
        var to = req.body.to;
        var from = req.body.from;


        var getYear = function (date) {
            return date.substring(0, date.indexOf("-"));
        }
        var getMonth = function (date) {
            return date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        }
        var getDay = function (date) {
            return date.substring(date.lastIndexOf("-") + 1, date.length);
        }

        /* validate "to" date */
        if (!validateDate(to))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");

        /* validate "from" date */
        if (!validateDate(from))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");

        var toDate = new Date(getYear(to), getMonth(to) - 1, getDay(to), 23, 59, 59);
        toDate = toDate.getTime();
        console.log(toDate);

        var fromDate = new Date(getYear(from), getMonth(from) - 1, getDay(from), 0, 0, 0);
        fromDate = fromDate.getTime();
        console.log(fromDate);
        if (fromDate > toDate) {
            res.send("Start date must be less than End date.")
        } else {
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_USERORDERS";
                db.collection(tableName).find({
                    "Timestamp": {
                        $gte: fromDate,
                        $lte: toDate
                    }
                }).toArray(function (err, result) {
                    if (err) {
                        res.json({"error": "Something went wrong."});
                    } else {
                        res.json(result);
                    }
                });
            });

        }


    } else {
        res.json({"error": "Missing parameters"});
    }

}
exports.createBigDealMerchantTest = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.Account && req.body.POC && req.body.restname && req.body.email) {

        var email = req.body.email;
        var phn = req.body.phn;
        var restname = req.body.restname;
        var restLogo = req.body.restLogo;
        var fax = req.body.fax;
        var lat = req.body.lat;
        var long = req.body.long;
        var restAdd = req.body.restAdd;
        var minTmDelivery = req.body.minTmDelivery;
        var minOrderAmt = req.body.minOrderAmt;
        var deliveryCharges = req.body.deliveryCharges;
        var discount = req.body.discount;
        var pickUpTime = req.body.pickUpTime;
        var deliveryAvailable = req.body.deliveryAvailable;
        var pickupAvailable = req.body.pickupAvailable;
        var POC = req.body.POC;
        var maxDistance = req.body.maxDistance;
        var Account = req.body.Account;
        var workingHrs = req.body.workingHrs;
        var couponDesc = req.body.couponDesc;
        var couponDiscount = req.body.couponDiscount;
        var rating = req.body.rating;
        var priceRange = req.body.priceRange;
        var tenant = req.body.tenant;
        var restCuisine = req.body.restCuisine;
        var vat = req.body.vat;
        var serviceCharge = req.body.serviceCharge;
        var serviceTax = req.body.serviceTax;
        var merchantDiscount = req.body.merchantDiscount;
        var appid = req.body.appid;
        var featureId = req.body.featureId;
        var restId = req.body.restId;
        var callOnPickUp = req.body.callOnPickUp;
        var callOnDelivery = req.body.callOnDelivery;
        var notificationOnPickUp = req.body.notificationOnPickUp;
        var notificationOnDelivery = req.body.notificationOnDelivery;
        var smsOnPickUp = req.body.smsOnPickUp;
        var smsOnDelivery = req.body.smsOnDelivery;
        var paytmOnPickUp = req.body.paytmOnPickUp;
        var paytmOnDelivery = req.body.paytmOnDelivery;
        var codOnPickUp = req.body.codOnPickUp;
        var codOnDelivery = req.body.codOnDelivery;
        var supportReadyAlert = req.body.supportReadyAlert;
        var supportDeliveryAlert = req.body.supportDeliveryAlert;
        var others = req.body.others;

        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_MERCHANTS";
                db.collection(tableName).find({
                    "email": email,
                    "restId": restId
                }).toArray(function (err, result) {
                    console.log(result);
                    if (result.length > 0) {
                        res.json({
                            "success": false, "message": "Merchant already exist."
                        });
                    } else {
                        var data = {
                            'email': email,
                            'phn': phn,
                            'restname': restname,
                            'fax': fax,
                            'lat': lat,
                            'long': long,
                            'restAdd': restAdd,
                            'restLogo': restLogo,
                            'minTmDelivery': minTmDelivery,
                            'minOrderAmt': minOrderAmt,
                            'deliveryCharges': deliveryCharges,
                            'discount': discount,
                            'pickUpTime': pickUpTime,
                            'deliveryAvailable': deliveryAvailable,
                            'pickupAvailable': pickupAvailable,
                            'POC': POC,
                            'maxDistance': maxDistance,
                            'Account': Account,
                            'workingHrs': workingHrs,
                            'couponDesc': couponDesc,
                            'couponDiscount': couponDiscount,
                            'rating': rating,
                            'priceRange': priceRange,
                            'tenant': tenant,
                            'restCuisine': restCuisine,
                            'vat': vat,
                            'serviceCharge': serviceCharge,
                            'serviceTax': serviceTax,
                            'merchantDiscount': merchantDiscount,
                            'appid': appid,
                            'featureId': featureId,
                            'restId': restId,
                            'others': others,
                            'callOnPickUp': callOnPickUp,
                            'callOnDelivery': callOnDelivery,
                            'notificationOnPickUp': notificationOnPickUp,
                            'notificationOnDelivery': notificationOnDelivery,
                            'smsOnPickUp': smsOnPickUp,
                            'smsOnDelivery': smsOnDelivery,
                            'paytmOnPickUp': paytmOnPickUp,
                            'paytmOnDelivery': paytmOnDelivery,
                            'codOnPickUp': codOnPickUp,
                            'codOnDelivery': codOnDelivery,
                            'supportReadyAlert': supportReadyAlert,
                            'supportDeliveryAlert': supportDeliveryAlert,
                            'username': restId,
                            'password': "12345"
                        };
                        console.log('data added');
                        db.collection(tableName).insertOne(data, function (err, result3) {
                            if (err) {
                                res.json({
                                    "success": false, "message": "Failed to add merchant."
                                });
                            } else {
                                res.json({
                                    "success": true, "message": "Merchant added successfully."
                                });
                            }
                        });
                    }
                });
            });
        } catch (e) {
            console.log(e)
            res.json({"error": e});
        }
    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }
}
exports.updateBigDealMerchantTest = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.Account && req.body.POC && req.body.restname && req.body.email) {

        var email = req.body.email;
        var phn = req.body.phn;
        var restname = req.body.restname;
        var restLogo = req.body.restLogo;
        var fax = req.body.fax;
        var lat = req.body.lat;
        var long = req.body.long;
        var restAdd = req.body.restAdd;
        var minTmDelivery = req.body.minTmDelivery;
        var minOrderAmt = req.body.minOrderAmt;
        var deliveryCharges = req.body.deliveryCharges;
        var discount = req.body.discount;
        var pickUpTime = req.body.pickUpTime;
        var deliveryAvailable = req.body.deliveryAvailable;
        var pickupAvailable = req.body.pickupAvailable;
        var POC = req.body.POC;
        var maxDistance = req.body.maxDistance;
        var Account = req.body.Account;
        var workingHrs = req.body.workingHrs;
        var couponDesc = req.body.couponDesc;
        var couponDiscount = req.body.couponDiscount;
        var rating = req.body.rating;
        var priceRange = req.body.priceRange;
        var tenant = req.body.tenant;
        var restCuisine = req.body.restCuisine;
        var vat = req.body.vat;
        var serviceCharge = req.body.serviceCharge;
        var serviceTax = req.body.serviceTax;
        var merchantDiscount = req.body.merchantDiscount;
        var appid = req.body.appid;
        var featureId = req.body.featureId;
        var restId = req.body.restId;
        var callOnPickUp = req.body.callOnPickUp;
        var callOnDelivery = req.body.callOnDelivery;
        var notificationOnPickUp = req.body.notificationOnPickUp;
        var notificationOnDelivery = req.body.notificationOnDelivery;
        var smsOnPickUp = req.body.smsOnPickUp;
        var smsOnDelivery = req.body.smsOnDelivery;
        var paytmOnPickUp = req.body.paytmOnPickUp;
        var paytmOnDelivery = req.body.paytmOnDelivery;
        var codOnPickUp = req.body.codOnPickUp;
        var codOnDelivery = req.body.codOnDelivery;
        var supportReadyAlert = req.body.supportReadyAlert;
        var supportDeliveryAlert = req.body.supportDeliveryAlert;
        var username = req.body.username;
        var password = req.body.password;
        var others = req.body.others;

        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_MERCHANTS";
                db.collection(tableName).find({
                    "email": email,
                    "restId": restId
                }).toArray(function (err, result) {
                    console.log(result);
                    if (result.length > 0) {
                        var data = result[0];
                        data.email = email;
                        data.phn = phn;
                        data.restname = restname;
                        data.fax = fax;
                        data.lat = lat;
                        data.long = long;
                        data.restAdd = restAdd;
                        data.restLogo = restLogo;
                        data.minTmDelivery = minTmDelivery;
                        data.minOrderAmt = minOrderAmt;
                        data.deliveryCharges = deliveryCharges;
                        data.discount = discount;
                        data.pickUpTime = pickUpTime;
                        data.deliveryAvailable = deliveryAvailable;
                        data.pickupAvailable = pickupAvailable;
                        data.POC = POC;
                        data.maxDistance = maxDistance;
                        data.Account = Account;
                        data.workingHrs = workingHrs;
                        data.couponDesc = couponDesc;
                        data.couponDiscount = couponDiscount;
                        data.rating = rating;
                        data.priceRange = priceRange;
                        data.tenant = tenant;
                        data.restCuisine = restCuisine;
                        data.vat = vat;
                        data.serviceCharge = serviceCharge;
                        data.serviceTax = serviceTax;
                        data.merchantDiscount = merchantDiscount;
                        data.appid = appid;
                        data.featureId = featureId;
                        data.restId = restId;
                        data.others = others;
                        data.callOnPickUp = callOnPickUp;
                        data.callOnDelivery = callOnDelivery;
                        data.notificationOnPickUp = notificationOnPickUp;
                        data.notificationOnDelivery = notificationOnDelivery;
                        data.smsOnPickUp = smsOnPickUp;
                        data.smsOnDelivery = smsOnDelivery;
                        data.paytmOnPickUp = paytmOnPickUp;
                        data.paytmOnDelivery = paytmOnDelivery;
                        data.codOnPickUp = codOnPickUp;
                        data.codOnDelivery = codOnDelivery;
                        data.supportReadyAlert = supportReadyAlert;
                        data.supportDeliveryAlert = supportDeliveryAlert;
                        data.username = username;
                        data.password = password;
                        db.collection(tableName).updateOne({'_id': data._id}, {$set: data}, function (err, result) {
                            if (err) {
                                res.json({"success": false, "message": "Failed to update merchant."})
                            } else {
                                res.json({"success": true, "message": "Merchant updated successfully."})
                            }
                        });

                    } else {
                        res.json({
                            "success": false, "message": "Merchant doesn't exist."
                        });
                    }
                });
            });
        } catch (e) {
            console.log(e)
            res.json({"error": e});
        }
    } else {
        res.status(401).json({
            "Error": "Parameters missing"
        });
    }
}

exports.merchantLoginTest = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.username && req.body.password) {
        var username = req.body.username;
        var password = req.body.password;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_MERCHANTS";
                db.collection(tableName).find({
                    'username': username
                }).toArray(function (err, result) {
                    if (result.length > 0) {
                        var merchantDetail = result[0];
                        if (merchantDetail.password == password) {
                            res.json({'success': true, 'merchantDetails': result});
                        } else {
                            res.json({"success": false, "message": "Invalid username and password."});
                        }
                    } else {
                        res.json({"success": false, "message": "Invalid username and password."});
                    }
                });
            });
        } catch (e) {
            console.log(e)
            res.json({"error": e});
        }


    } else {
        res.json({"error": "parameters missing username,password"});
    }
}

exports.getBigDealOrderStatusTest = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.orderId) {
        var orderId = req.body.orderId;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderId
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.json({"success": false, "message": "Order doesn't exist."});
                    } else {
                        var data = result[0];
                        res.json({"success": true, "status": data.status});

                    }
                });
            });

        } catch (e) {
        }


    } else {
        res.json({"error": "missing parameters."});
    }

}
exports.getBigDealOrdersDetailTest = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.orderID && req.body.emailID) {
        var orderId = req.body.orderID;
        var emailID = req.body.emailID;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderId,
                    'emailID': emailID
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.json({"success": false, "message": "Order doesn't exist."});
                    } else {
                        var data = result[0];
                        res.json({"success": true, "order": data});

                    }
                });
            });

        } catch (e) {
        }


    } else {
        res.json({"error": "missing parameters."});
    }

}


exports.getBigDealMerchantsTest = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.lat && req.body.long) {
        var tenant = req.body.tenant;
        try {
            var today = new Date();
            var date = moment.tz(today, "Asia/Kolkata").format();
            /* date will a sting like-  2016-07-08T18:09:18+05:30 */

            /* get year,month,day,hours,minutes from date string */
            var yyyy = date.substring(0, date.indexOf("-"));
            var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
            var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
            var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
            var minutes = date.substr(date.indexOf(":") + 1, 2);
            var currentDate = new Date(yyyy, mm - 1, dd, hours, minutes, 0, 0);
            today = yyyy + '-' + mm + '-' + dd;

            var tenant = req.body.tenant;
            var getMinutes = function (str) {
                var time = str.split(':');
                return time[0] * 60 + time[1] * 1;
            }
            var getMinutesNow = function (str) {

                return currentDate.getHours() * 60 + currentDate.getMinutes();
            }

            var checkRestaurantTiming = function (tm, now) {
                var found = false;
                try {
                    var times = tm.split("#");
                    console.log(times);
                    for (var k = 0; k < times.length; k++) {
                        var start = getMinutes(times[k].split(" - ")[0]);
                        var end = getMinutes(times[k].split(" - ")[1]);

                        if ((now >= start) && (now <= end)) {
                            found = true;
                            break;
                        } else {
                            found = false;
                        }
                    }
                } catch (e) {
                }
                console.log(found);
                return found;
            }
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_MERCHANTS";
                db.collection(tableName).find({
                    'tenant': tenant
                }).toArray(function (err, result) {
                    var MerchantDetails = [];
                    var openRestaurant = [];
                    var closedRestaurant = [];
                    if (result.length === 0) {
                        res.json({
                            "success": false, "message": "No merchant exists."
                        });
                    } else {
                        var day = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

                        for (var i = 0; i < result.length; i++) {
                            try {
                                console.log(i);

                                if (result[i].email) {

                                    var CurrentDay = day[currentDate.getDay()];
                                    var crtm = currentDate.getHours() + ":" + currentDate.getMinutes();
                                    console.log(CurrentDay + " " + crtm);
                                    var now = getMinutes(crtm);
                                    switch (CurrentDay) {
                                        case 'Mon': {
                                            var tm = result[i].workingHrs.monday;
                                            var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                            break;
                                        }
                                        case 'Tue': {
                                            var tm = result[i].workingHrs.tuesday;
                                            var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                            break;
                                        }
                                        case 'Wed': {
                                            var tm = result[i].workingHrs.wednesday;
                                            var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                            break;
                                        }
                                        case 'Thu': {
                                            var tm = result[i].workingHrs.thursday;
                                            var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                            break;
                                        }
                                        case 'Fri': {
                                            var tm = result[i].workingHrs.friday;
                                            var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                            break;
                                        }
                                        case 'Sat': {
                                            var tm = result[i].workingHrs.saturday;
                                            var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                            break;
                                        }
                                        case 'Sun': {
                                            var tm = result[i].workingHrs.sunday;
                                            var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                            break;
                                        }
                                    }
                                    if (RestaurantIsOpen == true) {
                                        try {
                                            var distance = geolib.getDistance({
                                                latitude: req.body.lat,
                                                longitude: req.body.long
                                            }, {
                                                latitude: result[i].lat,
                                                longitude: result[i].long
                                            });
                                        } catch (e) {
                                        }
                                        result[i].distance = distance;
                                        result[i].open = "Yes";
                                        openRestaurant.push(result[i]);
                                    } else {
                                        var distance = geolib.getDistance({
                                            latitude: req.body.lat,
                                            longitude: req.body.long
                                        }, {
                                            latitude: result[i].lat,
                                            longitude: result[i].long
                                        });
                                        result[i].distance = distance;
                                        result[i].open = "No";
                                        closedRestaurant.push(result[i]);
                                    }

                                } else {
                                    //result.splice(i, 1);
                                }
                            } catch (e) {
                                console.log(e)
                            }
                        }
                        MerchantDetails = openRestaurant;
                        MerchantDetails.sort(sortBy('distance'));
                        res.json({
                            "success": true,
                            "openRestaurant": MerchantDetails,
                            "closedRestaurant": closedRestaurant
                        });
                    }
                });
            });
        } catch (e) {
            res.json({"error": "Something went wrong."});
        }
    } else {
        res.status(401).json({
            "Error": "Body should contain tenant, lat, long"
        });
    }
};
exports.getBigDealCanteenMerchantsTest = function (req, res, next) {
    if (req.body && req.body.tenant) {

        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a string like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);
        var currentDate = new Date(yyyy, mm - 1, dd, hours, minutes, 0, 0);
        var tenant = req.body.tenant;
        var getMinutes = function (str) {
            var time = str.split(':');
            return time[0] * 60 + time[1] * 1;
        }
        var getMinutesNow = function (str) {

            return currentDate.getHours() * 60 + currentDate.getMinutes();
        }
        var checkRestaurantTiming = function (tm, now) {
            var found = false;
            try {
                var times = tm.split("#");
                console.log(times);
                for (var k = 0; k < times.length; k++) {
                    var start = getMinutes(times[k].split(" - ")[0]);
                    var end = getMinutes(times[k].split(" - ")[1]);

                    if ((now >= start) && (now <= end)) {
                        found = true;
                        break;
                    } else {
                        found = false;
                    }
                }
            } catch (e) {
            }
            console.log(found);
            return found;
        }
        dbUtil.getConnection(function (db) {
            var tableName = "T_DUMMY_BIGDEAL_MERCHANTS";
            db.collection(tableName).find({
                'tenant': tenant,
                'restId': {$regex: /.*canteen.*/i}
            }).toArray(function (err, result) {
                var MerchantDetails = [];
                var openRestaurant = [];
                var closedRestaurant = [];
                if (result.length === 0) {
                    res.json({
                        "success": false, "message": "No merchant exists."
                    });
                } else {
                    var day = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

                    for (var i = 0; i < result.length; i++) {
                        try {
                            console.log(i);

                            if (result[i].email) {

                                var CurrentDay = day[currentDate.getDay()];
                                var crtm = currentDate.getHours() + ":" + currentDate.getMinutes();
                                console.log(CurrentDay + " " + crtm);
                                var now = getMinutes(crtm);
                                switch (CurrentDay) {
                                    case 'Mon': {
                                        var tm = result[i].workingHrs.monday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                    case 'Tue': {
                                        var tm = result[i].workingHrs.tuesday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                    case 'Wed': {
                                        var tm = result[i].workingHrs.wednesday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                    case 'Thu': {
                                        var tm = result[i].workingHrs.thursday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                    case 'Fri': {
                                        var tm = result[i].workingHrs.friday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                    case 'Sat': {
                                        var tm = result[i].workingHrs.saturday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                    case 'Sun': {
                                        var tm = result[i].workingHrs.sunday;
                                        var RestaurantIsOpen = checkRestaurantTiming(tm, now);
                                        break;
                                    }
                                }
                                if (RestaurantIsOpen == true) {
                                    try {
                                        var distance = geolib.getDistance({
                                            latitude: req.body.lat,
                                            longitude: req.body.long
                                        }, {
                                            latitude: result[i].lat,
                                            longitude: result[i].long
                                        });
                                    } catch (e) {
                                    }
                                    result[i].distance = distance;
                                    result[i].open = "Yes";
                                    openRestaurant.push(result[i]);
                                } else {
                                    /*var distance = geolib.getDistance({
                                     latitude: req.body.lat,
                                     longitude: req.body.long
                                     }, {
                                     latitude: result[i].lat,
                                     longitude: result[i].long
                                     });
                                     result[i].distance = distance;*/
                                    result[i].open = "No";
                                    closedRestaurant.push(result[i]);
                                }

                            } else {
                                //result.splice(i, 1);
                            }
                        } catch (e) {
                            console.log(e)
                        }
                    }
                    MerchantDetails = openRestaurant;
                    MerchantDetails.sort(sortBy('distance'));
                    res.json({
                        "success": true,
                        "openRestaurant": MerchantDetails,
                        "closedRestaurant": closedRestaurant
                    });
                }
            });
        });


    } else {
        res.json({"success": false, "message": "Missing parameters."});
    }
}


exports.merchantResponse = function (req, res, next) {

    var called_number = req.query.called_number;
    var sms_number = req.query.sms_number;
    var menu = req.query.menu;
    var call_date = req.query.call_date;
    var call_time = req.query.call_time;
    var call_status = req.query.call_status;
    var total_call_duration = req.query.total_call_duration;
    var orderID = req.query.order_id;
    if (menu != 'null') {
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderID
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.send("Data push failure. OrderID doesn't exist.");
                    } else {
                        var data = result[0];
                        if (menu == 1)
                            data.status = "confirmed";
                        else if (menu == 2)
                            data.status = "declined";
                        db.collection(tableName).updateOne({'_id': data._id}, {$set: data}, function (err, result) {
                            if (err) {
                                res.send("Data push failure.");
                            } else {
                                res.send("Data push successfully.");
                            }
                        });

                    }
                });
            });

        } catch (e) {
        }

    } else {
        res.send("Data push failure.Merchant didn't respond.");
    }
}

/* Merchant Response from Merchant App*/
exports.merchantOrderResponseTest = function (req, res, next) {

    if (req.body && req.body.tenant && req.body.emailID && req.body.orderID && req.body.status) {

        var emailID = req.body.emailID;
        var orderID = req.body.orderID;
        var order_status = req.body.status;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderID,
                    'emailID': emailID
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.json({'success': false, 'message': "Order doesn't exists."});
                    } else {
                        var data = result[0];
                        if (order_status == 'confirmed')
                            data.status = "confirmed";
                        else if (order_status == 'declined')
                            data.status = "declined";
                        db.collection(tableName).updateOne({'_id': data._id}, {$set: data}, function (err, result) {
                            if (err) {
                                console.log(JSON.stringify(err));
                                res.json({'success': false, 'message': "Something went wrong."});
                            } else {
                                if (order_status == 'confirmed')
                                    res.json({'success': true, 'message': "Order has been successfully confirmed."});
                                else
                                    res.json({'success': true, 'message': "Order has been declined."});
                            }
                        });

                    }
                });
            });

        } catch (e) {
        }
    }
    else {
        res.json({'success': false, 'message': "Missing paramters."});

    }

}
exports.merchantOrderStatusUpdateTest = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.orderID && req.body.emailID) {
        var orderId = req.body.orderID;
        var emailID = req.body.emailID;

        var ready = "";
        var delivered = "";
        if (req.body.ready)
            ready = req.body.ready;
        if (req.body.delivered)
            delivered = req.body.delivered;
        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_USERORDERS";
                db.collection(tableName).find({
                    'orderID': orderId,
                    'emailID': emailID
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        res.json({"success": false, "message": "Order doesn't exist."});
                    } else {
                        var data = result[0];
                        if (ready && ready == 'Yes') {
                            data.ready = ready;
                        } else if (delivered && delivered == 'Yes') {
                            data.delivered = delivered;
                        }
                        db.collection(tableName).updateOne({'_id': data._id}, {$set: data}, function (err, result) {
                            if (err) {
                                console.log(JSON.stringify(err));
                                res.json({'success': false, 'message': "Something went wrong."});
                            } else {
                                if (ready)
                                    res.json({'success': true, 'message': "Order is ready."});
                                else
                                    res.json({'success': true, 'message': "Order id delivered."});
                            }
                        });


                    }
                });
            });

        } catch (e) {
        }


    } else {
        res.json({"error": "missing parameters."});
    }

}


/*************** Analytics for BigDeal Production ************/


exports.getPageCountAnalytics = function (req, res, next) {
    if (req.body && req.body.to && req.body.from) {
        var to = req.body.to;
        var from = req.body.from;
        try {


            var getYear = function (date) {
                return date.substring(0, date.indexOf("-"));
            };
            var getMonth = function (date) {
                return date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
            };
            var getDay = function (date) {
                return date.substring(date.lastIndexOf("-") + 1, date.length);
            };


            /* validate "to" date */
            if (!validateDate(to))
                return res.send("Invalid date, date must be of format YYYY-MM-DD.");

            /* validate "from" date */
            if (!validateDate(from))
                return res.send("Invalid date, date must be of format YYYY-MM-DD.");

            var toDate = new Date(getYear(to), getMonth(to) - 1, getDay(to), 23, 59, 59);
            toDate = toDate.getTime();

            var fromDate = new Date(getYear(from), getMonth(from) - 1, getDay(from), 0, 0, 0);
            fromDate = fromDate.getTime();

            if (fromDate > toDate) {
                res.send("Start date must be less that End date.")
            } else {

                dbUtil.getConnection(function (db) {
                    var tableName = "T_BIGDEAL_PAGECOUNT_ANALYTICS";
                    db.collection(tableName).find({
                        "Timestamp": {
                            $gte: fromDate,
                            $lt: toDate
                        }
                    }).toArray(function (err, result) {
                        if (err) {
                            res.json({"error": "Something went wrong."});
                        } else {
                            res.json(result);
                        }
                    });
                });
            }
        } catch (e) {
            res.json({"error": "Something went wrong."});
        }

    } else {
        res.json({"error": "Missing parameters"});
    }

}

exports.updatePageCountAnalytics = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.page) {
        var page = req.body.page;

        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a sting like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);

        var currentDate = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
        today = yyyy + '-' + mm + '-' + dd;

        try {
            var getObjectToStore = function (param, page) {
                switch (page) {
                    case 'CategoryPage':
                        var count = param.CategoryPage;
                        param.CategoryPage = count + 1;
                        break;
                    case 'DineInPage':
                        var count = param.DineInPage;
                        param.DineInPage = count + 1;
                        break;

                    case 'DeliveryPage':
                        var count = param.DeliveryPage;
                        param.DeliveryPage = count + 1;
                        break;

                    case 'CouponBestDealPage':
                        var count = param.CouponBestDealPage;
                        param.CouponBestDealPage = count + 1;
                        break;

                    case 'ViewAllCouponPage':
                        var count = param.ViewAllCouponPage;
                        param.ViewAllCouponPage = count + 1;
                        break;

                    case 'CartPage':
                        var count = param.CartPage;
                        param.CartPage = count + 1;
                        break;

                    case 'PayNowPage':
                        var count = param.PayNowPage;
                        param.PayNowPage = count + 1;
                        break;

                }
                return param;

            };
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_PAGECOUNT_ANALYTICS";
                db.collection(tableName).find({
                    'Date': today
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        var data = {
                            "CategoryPage": 0,
                            "DineInPage": 0,
                            "DeliveryPage": 0,
                            "CouponBestDealPage": 0,
                            "ViewAllCouponPage": 0,
                            "CartPage": 0,
                            "PayNowPage": 0,
                            "Date": today,
                            "Timestamp": currentDate.getTime()
                        };

                        data = getObjectToStore(data, page);
                        db.collection(tableName).insertOne(data, function (err, result) {
                            res.json({
                                "success": "Record entered successfully."
                            });
                        });

                    } else {
                        var data = result[0];
                        data = getObjectToStore(data, page);
                        db.collection(tableName).updateOne({"_id": data._id}, {$set: data}, function (err, result) {
                            if (err) {
                                res.json({
                                    "error": "Error."
                                });
                            }
                            else
                                res.json({
                                    "success": "Record updated successfully."
                                });
                        });

                    }
                });
            });
        }
        catch (e) {

        }
    } else {
        res.send("Missing parameters");
    }
};
exports.updateRestCountAnalytics = function (req, res, next) {
    if (req.body && req.body.restname) {
        var restName = req.body.restname;
        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a sting like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        console.log(date);
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);


        /* var currentDate = new Date(yyyy, mm - 1, 14, 0, 0, 0, 0);
         today = "2016-07-14";*/
        var currentDate = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
        today = yyyy + '-' + mm + '-' + dd;

        try {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_RESTCOUNT_ANALYTICS";
                db.collection(tableName).find({
                    'Date': today,
                    'RestName': restName
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        var data = {
                            "RestName": restName,
                            "Count": 1,
                            "Date": today,
                            "Timestamp": currentDate.getTime()
                        };

                        db.collection(tableName).insertOne(data, function (err, result) {
                            res.json({
                                "success": "Record entered successfully."
                            });
                        });

                    } else {
                        var data = result[0];
                        data.Count = data.Count + 1;
                        db.collection(tableName).updateOne({"_id": data._id}, {$set: data}, function (err, result) {
                            if (err) {
                                res.json({
                                    "error": "Error."
                                });
                            }
                            else
                                res.json({
                                    "success": "Record updated successfully."
                                });
                        });

                    }
                });
            });

        } catch (e) {
            res.json({"error": "Something went wrong."});
        }
    } else {
        res.json({"error": "Missing parameters."});
    }
};
exports.getRestCountAnalytics = function (req, res, next) {
    if (req.body && req.body.to && req.body.from) {
        var to = req.body.to;
        var from = req.body.from;
        try {

            var getYear = function (date) {
                return date.substring(0, date.indexOf("-"));
            }
            var getMonth = function (date) {
                return date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
            }
            var getDay = function (date) {
                return date.substring(date.lastIndexOf("-") + 1, date.length);
            }

            /* validate "to" date */
            if (!validateDate(to))
                return res.send("Invalid date, date must be of format YYYY-MM-DD.");

            /* validate "from" date */
            if (!validateDate(from))
                return res.send("Invalid date, date must be of format YYYY-MM-DD.");

            var toDate = new Date(getYear(to), getMonth(to) - 1, getDay(to), 23, 59, 59);
            toDate = toDate.getTime();
            console.log(toDate);

            var fromDate = new Date(getYear(from), getMonth(from) - 1, getDay(from), 0, 0, 0);
            fromDate = fromDate.getTime();
            console.log(fromDate);

            if (fromDate > toDate)
                return res.send("Start date must be less that End date.");

            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_RESTCOUNT_ANALYTICS";
                db.collection(tableName).find({
                    "Timestamp": {
                        $gte: fromDate,
                        $lte: toDate
                    }
                }).toArray(function (err, result) {
                    if (err) {
                        res.json({"error": "Something went wrong."});
                    } else {
                        res.json(result);
                    }
                });
            });
        } catch (e) {
            res.json({"error": "Something went wrong."});
        }
    } else {
        res.json({"error": "Missing parameters"});
    }

}
exports.updateCouponCountAnalytics = function (req, res, next) {
    if (req.body && req.body.brand) {
        var brand = req.body.brand;
        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a sting like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);


        /* var currentDate = new Date(yyyy, mm - 1, 14, 0, 0, 0, 0);
         today = "2016-07-14";*/
        var currentDate = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
        today = yyyy + '-' + mm + '-' + dd;

        try {


            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_GETCODECOUNT_ANALYTICS";
                db.collection(tableName).find({
                    'Date': today,
                    'Brand': brand
                }).toArray(function (err, result) {
                    console.log(JSON.stringify(result));
                    if (result.length == 0) {
                        var data = {
                            "Brand": brand,
                            "Count": 1,
                            "Date": today,
                            "Timestamp": currentDate.getTime()
                        };

                        db.collection(tableName).insertOne(data, function (err, result) {
                            res.json({
                                "success": "Record entered successfully."
                            });
                        });

                    } else {
                        var data = result[0];
                        data.Count = data.Count + 1;
                        db.collection(tableName).updateOne({"_id": data._id}, {$set: data}, function (err, result) {
                            if (err) {
                                res.json({
                                    "error": err
                                });
                            }
                            else
                                res.json({
                                    "success": "Record updated successfully."
                                });
                        });

                    }
                });
            });
        } catch (e) {
            res.json({"error": "Something went wrong."});
        }

    } else {
        res.json({"error": "Missing parameters."});
    }
};
exports.getCouponCountAnalytics = function (req, res, next) {
    if (req.body && req.body.to && req.body.from) {
        var to = req.body.to;
        var from = req.body.from;
        try {


            var getYear = function (date) {
                return date.substring(0, date.indexOf("-"));
            };

            var getMonth = function (date) {
                return date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
            };

            var getDay = function (date) {
                return date.substring(date.lastIndexOf("-") + 1, date.length);
            };


            /* validate "to" date */
            if (!validateDate(to))
                return res.send("Invalid date, date must be of format YYYY-MM-DD.");

            /* validate "from" date */
            if (!validateDate(from))
                return res.send("Invalid date, date must be of format YYYY-MM-DD.");

            var toDate = new Date(getYear(to), getMonth(to) - 1, getDay(to), 23, 59, 59);
            toDate = toDate.getTime();
            console.log(toDate);

            var fromDate = new Date(getYear(from), getMonth(from) - 1, getDay(from), 0, 0, 0);
            fromDate = fromDate.getTime();
            console.log(fromDate);

            if (fromDate > toDate) {
                res.send("Start date must be less that End date.")
            } else {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_BIGDEAL_GETCODECOUNT_ANALYTICS";
                    db.collection(tableName).find({
                        "Timestamp": {
                            $gte: fromDate,
                            $lte: toDate
                        }
                    }).toArray(function (err, result) {
                        if (err) {
                            res.json({"error": "Something went wrong."});
                        } else {
                            res.json(result);
                        }
                    });
                });

            }
        } catch (e) {
            res.json({"error": "Something went wrong."});
        }

    } else {
        res.json({"error": "Missing parameters"});
    }

}
exports.addBigDealAvailCoupon = function (req, res, next) {

    if (req.body && req.body.username && req.body.emailID && req.body.brand && req.body.title) {
        var username = req.body.username;
        var emailID = req.body.emailID;
        var brand = req.body.brand;
        var title = req.body.title;

        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a sting like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);


        /* var currentDate = new Date(yyyy, mm - 1, 14, 0, 0, 0, 0);
         today = "2016-07-14";*/
        var currentDate = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
        today = yyyy + '-' + mm + '-' + dd;
        var data = {
            "username": username,
            "emailID": emailID,
            "brand": brand,
            "title": title,
            "date": today,
            "time": hours + ":" + minutes,
            "Timestamp": currentDate.getTime()

        }
        dbUtil.getConnection(function (db) {
            var tableName = "T_BIGDEAL_AVAILCOUPON_ANALYTICS";

            db.collection(tableName).insertOne(data, function (err, result) {
                if (err) {
                    res.send("Error.");
                } else {
                    res.json({
                        "success": "Record entered successfully."
                    });

                }
            });

        });

    } else {
        res.send("Missing parameters.");
    }


};

exports.getBigDealAvailCoupon = function (req, res, next) {
    if (req.body && req.body.to && req.body.from) {
        var to = req.body.to;
        var from = req.body.from;


        var getYear = function (date) {
            return date.substring(0, date.indexOf("-"));
        }
        var getMonth = function (date) {
            return date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        }
        var getDay = function (date) {
            return date.substring(date.lastIndexOf("-") + 1, date.length);
        }

        /* validate "to" date */
        if (!validateDate(to))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");

        /* validate "from" date */
        if (!validateDate(from))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");

        var toDate = new Date(getYear(to), getMonth(to) - 1, getDay(to), 23, 59, 59);
        toDate = toDate.getTime();
        console.log(toDate);

        var fromDate = new Date(getYear(from), getMonth(from) - 1, getDay(from), 0, 0, 0);
        fromDate = fromDate.getTime();
        console.log(fromDate);
        if (fromDate > toDate) {
            res.send("Start date must be less than End date.")
        } else {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_AVAILCOUPON_ANALYTICS";
                db.collection(tableName).find({
                    "Timestamp": {
                        $gte: fromDate,
                        $lte: toDate
                    }
                }).toArray(function (err, result) {
                    if (err) {
                        res.json({"error": "Something went wrong."});
                    } else {
                        res.json(result);
                    }
                });
            });

        }


    } else {

    }

}

exports.saveBookStoreDetail = function (req, res) {
    if (req.body && req.body.username && req.body.emailID && req.body.tenant && req.body.phone) {
        var username = req.body.username;
        var emailID = req.body.emailID;
        var tenant = req.body.tenant;
        var phone = req.body.phone;

        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a sting like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);


        /* var currentDate = new Date(yyyy, mm - 1, 14, 0, 0, 0, 0);
         today = "2016-07-14";*/
        var currentDate = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
        today = yyyy + '-' + mm + '-' + dd;
        dbUtil.getConnection(function (db) {
            var tableName = "T_BIGDEAL_BOOKSTORE_ANALYTICS";
            var data = {
                "username": username,
                "emailID": emailID,
                "tenant": tenant,
                "phone": phone,
                "date": today,
                "time": hours + ":" + minutes,
                "Timestamp": currentDate.getTime()
            };

            db.collection(tableName).insertOne(data, function (err, result) {
                if (err) {
                    res.send("Error.");
                } else {
                    res.json({
                        "success": "Record entered successfully."
                    });

                }
            });

        });

    } else {
        res.json({"error": "Missing parameters"});
    }
}

exports.getBookDetail = function (req, res) {

    if (req.body && req.body.to && req.body.from && req.body.tenant) {
        var to = req.body.to;
        var from = req.body.from;
        var tenant = req.body.tenant;


        var getYear = function (date) {
            return date.substring(0, date.indexOf("-"));
        }
        var getMonth = function (date) {
            return date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        }
        var getDay = function (date) {
            return date.substring(date.lastIndexOf("-") + 1, date.length);
        }

        /* validate "to" date */
        if (!validateDate(to))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");

        /* validate "from" date */
        if (!validateDate(from))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");

        var toDate = new Date(getYear(to), getMonth(to) - 1, getDay(to), 23, 59, 59);
        toDate = toDate.getTime();
        console.log(toDate);

        var fromDate = new Date(getYear(from), getMonth(from) - 1, getDay(from), 0, 0, 0);
        fromDate = fromDate.getTime();
        console.log(fromDate);
        if (fromDate > toDate) {
            res.send("Start date must be less than End date.")
        } else {
            dbUtil.getConnection(function (db) {
                var tableName = "T_BIGDEAL_BOOKSTORE_ANALYTICS";
                db.collection(tableName).find({
                    'tenant': tenant,
                    "Timestamp": {
                        $gte: fromDate,
                        $lte: toDate
                    }
                }).toArray(function (err, result) {
                    if (err) {
                        res.json({"error": "Something went wrong."});
                    } else {
                        res.json(result);
                    }
                });
            });

        }


    } else {
        res.send('Parameters missing.');
    }

}


/***************Analytics for BigDeal QA ************/


exports.getPageCountAnalyticsTest = function (req, res, next) {
    if (req.body && req.body.to && req.body.from) {
        var to = req.body.to;
        var from = req.body.from;
        try {


            var getYear = function (date) {
                return date.substring(0, date.indexOf("-"));
            }
            var getMonth = function (date) {
                return date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
            }
            var getDay = function (date) {
                return date.substring(date.lastIndexOf("-") + 1, date.length);
            }

            /* validate "to" date */
            if (!validateDate(to))
                return res.send("Invalid date, date must be of format YYYY-MM-DD.");

            /* validate "from" date */
            if (!validateDate(from))
                return res.send("Invalid date, date must be of format YYYY-MM-DD.");


            var toDate = new Date(getYear(to), getMonth(to) - 1, getDay(to), 23, 59, 59);
            toDate = toDate.getTime();
            console.log(toDate);

            var fromDate = new Date(getYear(from), getMonth(from) - 1, getDay(from), 0, 0, 0);
            fromDate = fromDate.getTime();
            console.log(fromDate);

            if (fromDate > toDate) {
                res.send("Start date must be less than End date.")
            } else {
                dbUtil.getConnection(function (db) {
                    var tableName = "T_DUMMY_BIGDEAL_PAGECOUNT_ANALYTICS";
                    db.collection(tableName).find({
                        "Timestamp": {
                            $gte: fromDate,
                            $lte: toDate
                        }
                    }).toArray(function (err, result) {
                        if (err) {
                            res.json({"error": "Something went wrong."});
                        } else {
                            res.json(result);
                        }
                    });
                });

            }
        } catch (e) {
            res.json({"error": "Something went wrong."});
        }


    } else {
        res.json({"error": "Missing parameters"});
    }

}

exports.updatePageCountAnalyticsTest = function (req, res, next) {
    if (req.body && req.body.tenant && req.body.page) {
        var page = req.body.page;

        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a sting like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        console.log(date);
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);


        /* var currentDate = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
         today = "2016-07-14";*/
        var currentDate = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
        today = yyyy + '-' + mm + '-' + dd;
        console.log(currentDate);

        try {
            var getObjectToStore = function (param, page) {
                switch (page) {
                    case 'CategoryPage':
                        var count = param.CategoryPage;
                        param.CategoryPage = count + 1;
                        break;
                    case 'DineInPage':
                        var count = param.DineInPage;
                        param.DineInPage = count + 1;
                        break;

                    case 'DeliveryPage':
                        var count = param.DeliveryPage;
                        param.DeliveryPage = count + 1;
                        break;

                    case 'CouponBestDealPage':
                        var count = param.CouponBestDealPage;
                        param.CouponBestDealPage = count + 1;
                        break;

                    case 'ActivateCouponClick':
                        var count = param.ActivateCouponClick;
                        param.ActivateCouponClick = count + 1;
                        break;

                    case 'ViewAllCouponPage':
                        var count = param.ViewAllCouponPage;
                        param.ViewAllCouponPage = count + 1;
                        break;

                    case 'CartPage':
                        var count = param.CartPage;
                        param.CartPage = count + 1;
                        break;

                    case 'PayNowPage':
                        var count = param.PayNowPage;
                        param.PayNowPage = count + 1;
                        break;

                }
                return param;

            };
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_PAGECOUNT_ANALYTICS";
                db.collection(tableName).find({
                    'Date': today
                }).toArray(function (err, result) {
                    if (result.length == 0) {
                        var data = {
                            "CategoryPage": 0,
                            "DineInPage": 0,
                            "DeliveryPage": 0,
                            "CouponBestDealPage": 0,
                            "ActivateCouponClick": 0,
                            "ViewAllCouponPage": 0,
                            "CartPage": 0,
                            "PayNowPage": 0,
                            "Date": today,
                            "Timestamp": currentDate.getTime()
                        };

                        data = getObjectToStore(data, page);
                        db.collection(tableName).insertOne(data, function (err, result) {
                            res.json({
                                "success": "Record entered successfully."
                            });
                        });

                    } else {
                        var data = result[0];
                        data = getObjectToStore(data, page);
                        db.collection(tableName).updateOne({"_id": data._id}, {$set: data}, function (err, result) {
                            if (err) {
                                res.json({
                                    "error": "Error."
                                });
                            }
                            else
                                res.json({
                                    "success": "Record updated successfully."
                                });
                        });

                    }
                });
            });
        }
        catch (e) {

        }
    } else {
        res.send("Missing parameters");
    }
}

exports.updateRestCountAnalyticsTest = function (req, res, next) {
    if (req.body && req.body.restname) {
        var restName = req.body.restname;
        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a sting like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        console.log(date);
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);


        /* var currentDate = new Date(yyyy, mm - 1, 14, 0, 0, 0, 0);
         today = "2016-07-14";*/
        var currentDate = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
        today = yyyy + '-' + mm + '-' + dd;


        dbUtil.getConnection(function (db) {
            var tableName = "T_DUMMY_BIGDEAL_RESTCOUNT_ANALYTICS";
            db.collection(tableName).find({
                'Date': today,
                'RestName': restName
            }).toArray(function (err, result) {
                if (result.length == 0) {
                    var data = {
                        "RestName": restName,
                        "Count": 1,
                        "Date": today,
                        "Timestamp": currentDate.getTime()
                    };

                    db.collection(tableName).insertOne(data, function (err, result) {
                        res.json({
                            "success": "Record entered successfully."
                        });
                    });

                } else {
                    var data = result[0];
                    data.Count = data.Count + 1;
                    db.collection(tableName).updateOne({"_id": data._id}, {$set: data}, function (err, result) {
                        if (err) {
                            res.json({
                                "error": "Error."
                            });
                        }
                        else
                            res.json({
                                "success": "Record updated successfully."
                            });
                    });

                }
            });
        });


    } else {
        res.json({"error": "Missing parameters."});
    }
};
exports.getRestCountAnalyticsTest = function (req, res, next) {
    if (req.body && req.body.to && req.body.from) {
        var to = req.body.to;
        var from = req.body.from;

        var getYear = function (date) {
            return date.substring(0, date.indexOf("-"));
        }
        var getMonth = function (date) {
            return date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        }
        var getDay = function (date) {
            return date.substring(date.lastIndexOf("-") + 1, date.length);
        }

        /* validate "to" date */
        if (!validateDate(to))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");

        /* validate "from" date */
        if (!validateDate(from))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");

        var toDate = new Date(getYear(to), getMonth(to) - 1, getDay(to), 23, 59, 59);
        toDate = toDate.getTime();
        console.log(toDate);

        var fromDate = new Date(getYear(from), getMonth(from) - 1, getDay(from), 0, 0, 0);
        fromDate = fromDate.getTime();
        console.log(fromDate);
        if (fromDate > toDate) {
            res.send("Start date must be less than End date.")
        } else {
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_RESTCOUNT_ANALYTICS";
                db.collection(tableName).find({
                    "Timestamp": {
                        $gte: fromDate,
                        $lte: toDate
                    }
                }).toArray(function (err, result) {
                    if (err) {
                        res.json({"error": "Something went wrong."});
                    } else {
                        res.json(result);
                    }
                });
            });

        }


    } else {
        res.json({"error": "Missing parameters"});
    }

}
exports.updateCouponCountAnalyticsTest = function (req, res, next) {
    if (req.body && req.body.brand) {
        var brand = req.body.brand;
        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a sting like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);


        /* var currentDate = new Date(yyyy, mm - 1, 14, 0, 0, 0, 0);
         today = "2016-07-14";*/
        var currentDate = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
        today = yyyy + '-' + mm + '-' + dd;


        dbUtil.getConnection(function (db) {
            var tableName = "T_DUMMY_BIGDEAL_GETCODECOUNT_ANALYTICS";
            db.collection(tableName).find({
                'Date': today,
                'Brand': brand
            }).toArray(function (err, result) {
                console.log(JSON.stringify(result));
                if (result.length == 0) {
                    var data = {
                        "Brand": brand,
                        "Count": 1,
                        "Date": today,
                        "Timestamp": currentDate.getTime()
                    };

                    db.collection(tableName).insertOne(data, function (err, result) {
                        res.json({
                            "success": "Record entered successfully."
                        });
                    });

                } else {
                    var data = result[0];
                    data.Count = data.Count + 1;
                    db.collection(tableName).updateOne({"_id": data._id}, {$set: data}, function (err, result) {
                        if (err) {
                            res.json({
                                "error": err
                            });
                        }
                        else
                            res.json({
                                "success": "Record updated successfully."
                            });
                    });

                }
            });
        });


    } else {
        res.json({"error": "Missing parameters."});
    }
};
exports.getCouponCountAnalyticsTest = function (req, res, next) {
    if (req.body && req.body.to && req.body.from) {
        var to = req.body.to;
        var from = req.body.from;

        var getYear = function (date) {
            return date.substring(0, date.indexOf("-"));
        };
        var getMonth = function (date) {
            return date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        };
        var getDay = function (date) {
            return date.substring(date.lastIndexOf("-") + 1, date.length);
        };

        /* validate "to" date */
        if (!validateDate(to))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");

        /* validate "from" date */
        if (!validateDate(from))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");

        var toDate = new Date(getYear(to), getMonth(to) - 1, getDay(to), 23, 59, 59);
        toDate = toDate.getTime();
        console.log(toDate);

        var fromDate = new Date(getYear(from), getMonth(from) - 1, getDay(from), 0, 0, 0);
        fromDate = fromDate.getTime();
        console.log(fromDate);

        if (fromDate > toDate) {
            res.send("Start date must be less that End date.")
        } else {
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_GETCODECOUNT_ANALYTICS";
                db.collection(tableName).find({
                    "Timestamp": {
                        $gte: fromDate,
                        $lte: toDate
                    }
                }).toArray(function (err, result) {
                    if (err) {
                        res.json({"error": "Something went wrong."});
                    } else {
                        res.json(result);
                    }
                });
            });

        }


    } else {
        res.json({"error": "Missing parameters"});
    }

}

exports.addBigDealAvailCouponTest = function (req, res, next) {

    if (req.body && req.body.username && req.body.emailID && req.body.brand && req.body.title) {

        var username = req.body.username;
        var emailID = req.body.emailID;
        var brand = req.body.brand;
        var title = req.body.title;

        var today = new Date();
        var date = moment.tz(today, "Asia/Kolkata").format();
        /* date will a sting like-  2016-07-08T18:09:18+05:30   */

        /* get year,month,day,hours,minutes from date string */
        var yyyy = date.substring(0, date.indexOf("-"));
        var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
        var hours = date.substring(date.indexOf("T") + 1, date.indexOf(":"));
        var minutes = date.substr(date.indexOf(":") + 1, 2);


        /* var currentDate = new Date(yyyy, mm - 1, 14, 0, 0, 0, 0);
         today = "2016-07-14";*/
        var currentDate = new Date(yyyy, mm - 1, dd, 0, 0, 0, 0);
        today = yyyy + '-' + mm + '-' + dd;
        var data = {
            "username": username,
            "emailID": emailID,
            "brand": brand,
            "title": title,
            "date": today,
            "time": hours + ":" + minutes,
            "Timestamp": currentDate.getTime()

        }
        dbUtil.getConnection(function (db) {
            var tableName = "T_DUMMY_BIGDEAL_AVAILCOUPON_ANALYTICS";

            db.collection(tableName).insertOne(data, function (err, result) {
                if (err) {
                    res.send("Error.");
                } else {
                    res.json({
                        "success": "Record entered successfully."
                    });

                }
            });

        });

    } else {
        res.send("Missing parameters.");
    }


};
exports.getBigDealAvailCouponTest = function (req, res, next) {
    if (req.body && req.body.to && req.body.from) {
        var to = req.body.to;
        var from = req.body.from;


        var getYear = function (date) {
            return date.substring(0, date.indexOf("-"));
        }
        var getMonth = function (date) {
            return date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
        }
        var getDay = function (date) {
            return date.substring(date.lastIndexOf("-") + 1, date.length);
        }

        /* validate "to" date */
        if (!validateDate(to))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");

        /* validate "from" date */
        if (!validateDate(from))
            return res.send("Invalid date, date must be of format YYYY-MM-DD.");


        var toDate = new Date(getYear(to), getMonth(to) - 1, getDay(to), 23, 59, 59);
        toDate = toDate.getTime();
        console.log(toDate);

        var fromDate = new Date(getYear(from), getMonth(from) - 1, getDay(from), 0, 0, 0);
        fromDate = fromDate.getTime();
        console.log(fromDate);
        if (fromDate > toDate) {
            res.send("Start date must be less than End date.")
        } else {
            dbUtil.getConnection(function (db) {
                var tableName = "T_DUMMY_BIGDEAL_AVAILCOUPON_ANALYTICS";
                db.collection(tableName).find({
                    "Timestamp": {
                        $gte: fromDate,
                        $lte: toDate
                    }
                }).toArray(function (err, result) {
                    if (err) {
                        res.json({"error": "Something went wrong."});
                    } else {
                        res.json(result);
                    }
                });
            });

        }


    } else {
        res.json({"error": "Missing parameters"});
    }

}

function validateDate(dateToValidate) {

    var getYear = function (date) {
        return date.substring(0, date.indexOf("-"));
    }
    var getMonth = function (date) {
        return date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
    }
    var getDay = function (date) {
        return date.substring(date.lastIndexOf("-") + 1, date.length);
    }
    var dateYear = getYear(dateToValidate);
    if (dateYear.length != 4)
        return false;
    var dateMonth = getMonth(dateToValidate);
    if (dateMonth.length < 1 || dateMonth.length > 2 || parseInt(dateMonth) > 12 || parseInt(dateMonth) < 1)
        return false;
    var dateDay = getDay(dateToValidate);
    if (dateDay.length < 1 || dateDay.length > 2 || parseInt(dateDay) > 31 || parseInt(dateDay) < 1)
        return false;

    return true;

}


