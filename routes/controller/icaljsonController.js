var ical2json = require("ical2json");
var http = require('http');
var extrequest = require('request');
/**
 * Uploads a base 64 encoded image to the filesystem and returns the url.
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.convert = function(req, res, next) {
	var result = "";
	if (req.body && req.body.icalurl) {
		extrequest(req.body.icalurl, function(err, resp, body) {
			try {
				console.log(body);
				var output = ical2json.convert(body);
				console.log(output);
				res.json({"output":output});
			}catch(ex) {
				res.status(401).json({"Error":"Unable to parse ical data"});
			}
		});
	} else {
		res.status(401).json({"Error":"Body should contain 'icalurl'"});
	}
	
}
