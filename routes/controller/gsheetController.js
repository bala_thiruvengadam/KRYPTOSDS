var GoogleSpreadsheet = require("google-spreadsheet");
var bcrypt = require('bcryptjs');
var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var extrequest = require('request');

var smsurl = "http://203.129.203.243/blank/sms/user/urlsms.php?username=dealmonk&pass=112233&senderid=DLMONK&response=Y";
var smsHead = 'http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage';
//'&send_to=9717419431&msg=5024 is your One-Time Password (OTP) for this transaction.'
var smsTail = '&msg_type=TEXT&userid=2000161721&auth_scheme=plain&password=fu26CbwnO&v=1.1&format=text';
/**
 * 
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.finduser = function(req, res, next) {
	bcrypt.hash('bacon', 8, function(err, hash) {
		console.log("Hash : " + hash);
		bcrypt.compare('bacon2', hash, function(err, res) {
    		console.log("Compare " + res);
		});
	});
	if (req.body && req.body.sheetid && req.body.firstname && req.body.lastname && req.body.rollno ) {
		var result = "";
		var my_sheet = new GoogleSpreadsheet(req.body.sheetid);
		var firstname = req.body.firstname.toLowerCase();
		var lastname = req.body.lastname.toLowerCase();
		var rollno = req.body.rollno;
		var found = false;
		var founduser = {};
		my_sheet.getRows( 1, function(err, row_data){
			for(var i=0;i<row_data.length;i++) {
				var fname = row_data[i].firstname;
				var lname = row_data[i].lastname;
				var rnum = row_data[i].rollno;
				var email = row_data[i].email;
				var phone = row_data[i].phone;
				if(firstname === fname.toLowerCase() && lastname === lname.toLowerCase() && rollno === rnum ) {
					found = true;
					founduser.firstname = fname;
					founduser.lname = lname;
					founduser.rollno = rnum;
					founduser.email = email;
					founduser.phone = phone;
					break;
				}
				console.log(JSON.stringify(row_data[i].firstname));
			}
			console.log( 'pulled in '+row_data.length + ' rows');
			res.json({"found" : found, "userinfo" : founduser});
		});
		
	}else {
		res.status(401).json({"Error":"Body should contain 'sheetid', 'firstname', 'lastname', 'rollno'"});
	}
		
}


exports.finduserbyroll = function(req, res, next) {
	
	if (req.body && req.body.sheetid && req.body.rollno ) {
		var result = "";
		var my_sheet = new GoogleSpreadsheet(req.body.sheetid);
		var rollno = req.body.rollno;
		var found = false;
		var founduser = {};
		my_sheet.getRows( 1, function(err, row_data){
			for(var i=0;i<row_data.length;i++) {
				var fname = row_data[i].firstname;
				var lname = row_data[i].lastname;
				var rnum = row_data[i].rollno;
				var email = row_data[i].email;
				var phone = row_data[i].phone;
				if(rollno === rnum ) {
					found = true;
					founduser.firstname = fname;
					founduser.lname = lname;
					founduser.rollno = rnum;
					founduser.email = email;
					founduser.phone = phone;
					break;
				}
				console.log(JSON.stringify(row_data[i].firstname));
			}
			console.log( 'pulled in '+row_data.length + ' rows');
			res.json({"found" : found, "userinfo" : founduser});
		});
		
	}else {
		res.status(401).json({"Error":"Body should contain 'sheetid',  'rollno'"});
	}
		
}

exports.finduserbyemail = function(req, res, next) {
	
	if (req.body && req.body.sheetid && req.body.email ) {
		var result = "";
		var my_sheet = new GoogleSpreadsheet(req.body.sheetid);
		var remail = req.body.email;
		var found = false;
		var founduser = {};
		my_sheet.getRows( 1, function(err, row_data){
			for(var i=0;i<row_data.length;i++) {
				var fname = row_data[i].firstname;
				var lname = row_data[i].lastname;
				var rnum = row_data[i].rollno;
				var email = row_data[i].email;
				var phone = row_data[i].phone;
				if(remail.toLowerCase() === email.toLowerCase() ) {
					found = true;
					founduser.firstname = fname;
					founduser.lname = lname;
					founduser.rollno = rnum;
					founduser.email = email;
					founduser.phone = phone;
					break;
				}
				console.log(JSON.stringify(row_data[i].firstname));
			}
			console.log( 'pulled in '+row_data.length + ' rows');
			res.json({"found" : found, "userinfo" : founduser});
		});
		
	}else {
		res.status(401).json({"Error":"Body should contain 'sheetid',  'email'"});
	}
		
}


exports.sendOTP = function(req, res, next) {
	if (req.body && req.body.tenant  && req.body.sheetid && req.body.rollno ) {
		var result = "";
		var my_sheet = new GoogleSpreadsheet(req.body.sheetid);
		var rollno = req.body.rollno;
		var found = false;
		var founduser = {};
		var tenant = req.body.tenant;
		my_sheet.getRows( 1, function(err, row_data){
			for(var i=0;i<row_data.length;i++) {
				var fname = row_data[i].firstname;
				var lname = row_data[i].lastname;
				var rnum = row_data[i].rollno;
				var email = row_data[i].email;
				var phone = row_data[i].phone;
				if(rollno === rnum ) {
					found = true;
					founduser.firstname = fname;
					founduser.lastname = lname;
					founduser.rollno = rnum;
					founduser.email = email;
					founduser.phone = phone;
					break;
				}
				console.log(JSON.stringify(row_data[i].firstname));
			}
			console.log( 'pulled in '+row_data.length + ' rows');
			if(found) {
				dbUtil.getConnection(function(db){
					var tableName = "T_" + req.body.tenant + "_OTP";
					db.collection(tableName).find({"tenant" : tenant, "rollno": rollno}).toArray(function(err, result){
			 			if (!result || result.length === 0) {
							var OTP = Math.floor(Math.random()*90000) + 10000;
							var otprecord = {"tenant" : tenant, "rollno": rollno, "otp" : OTP};
							otprecord.firstname = founduser.firstname
							otprecord.lastname = founduser.lastname;
							otprecord.email = founduser.email;
							otprecord.phone = founduser.phone;
			 				db.collection(tableName).insertOne(otprecord, function(err, result) {
			    				console.log("Inserted a document into the " + tableName + " collection.");
								var url = smsHead + "&send_to=" + otprecord.phone + "&msg=" + OTP + " is your One-Time Password (OTP) for " + tenant + smsTail;
			    				extrequest(url, function(err, resp, body) {
			    					console.log("SMS Resp : " + body);	
			    					res.json(otprecord);
			    				});
			  				});
			 			} else {
			 				console.log("result.length " + result.length);
			 				var record = result[0];
			 				var id = new ObjectId(record._id);
			 				var OTP = Math.floor(Math.random()*90000) + 10000;
			 				record.otp = OTP;
			 				console.log(record);
			 				delete record._id;
			 				db.collection(tableName).replaceOne({"_id": id}, record, function(err, result2){
								console.log("Updating the record with id "+id);
								var url = smsHead + "&send_to=" + otprecord.phone + "&msg=" + OTP + " is your One-Time Password (OTP) for " + tenant + smsTail;
			    				extrequest(url, function(err, resp, body) {
			    					console.log("SMS Resp : " + body);
			    					res.json(record);
			    				});
							});							
						}
					});
				});
			}
			//res.json({"found" : found, "userinfo" : founduser});
		});
		
	}else {
		res.status(401).json({"Error":"Body should contain 'sheetid',  'rollno', 'tenant'"});
	}
		
}


exports.validateOTP = function(req, res, next) {
	if (req.body && req.body.tenant  && req.body.otp && req.body.rollno ) {
		var rollno = req.body.rollno;
		var tenant = req.body.tenant;
		var otp = req.body.otp;
		dbUtil.getConnection(function(db){
			var tableName = "T_" + req.body.tenant + "_OTP";
			db.collection(tableName).find({"tenant" : tenant, "rollno": rollno, "otp" : otp}).toArray(function(err, result){
				if (!result || result.length === 0) {
					res.json({"otpvalid" : false});
				}else {
					res.json({"otpvalid" : true});
				}
			});
		});
	}else {
		res.status(401).json({"Error":"Body should contain 'otp',  'rollno', 'tenant'"});
	}
}

exports.sendEmailOTP = function(req, res, next) {
	if (req.body && req.body.tenant  && req.body.sheetid && req.body.email ) {
		var result = "";
		var my_sheet = new GoogleSpreadsheet(req.body.sheetid);
		var remail = req.body.email;
		var found = false;
		var founduser = {};
		var tenant = req.body.tenant;
		my_sheet.getRows( 1, function(err, row_data){
			for(var i=0;i<row_data.length;i++) {
				var fname = row_data[i].firstname;
				var lname = row_data[i].lastname;
				var rnum = row_data[i].rollno;
				var email = row_data[i].email;
				var phone = row_data[i].phone;
				if(remail === email ) {
					found = true;
					founduser.firstname = fname;
					founduser.lastname = lname;
					founduser.rollno = rnum;
					founduser.email = email;
					founduser.phone = phone;
					break;
				}
				console.log(JSON.stringify(row_data[i].firstname));
			}
			console.log( 'pulled in '+row_data.length + ' rows');
			if(found) {
				dbUtil.getConnection(function(db){
					var tableName = "T_" + req.body.tenant + "_EMAIL_OTP";
					db.collection(tableName).find({"tenant" : tenant, "email": remail}).toArray(function(err, result){
			 			if (!result || result.length === 0) {
							var OTP = Math.floor(Math.random()*90000) + 10000;
							var otprecord = {"tenant" : tenant, "email": remail, "otp" : OTP};
							otprecord.firstname = founduser.firstname
							otprecord.lastname = founduser.lastname;
							otprecord.rollno = founduser.rollno;
							otprecord.phone = founduser.phone;
			 				db.collection(tableName).insertOne(otprecord, function(err, result) {
			    				console.log("Inserted a document into the " + tableName + " collection.");
			    				var msg = OTP + " is your One-Time Password (OTP) for " + tenant;
		    					var emaildata = {"from": "donotreply@kryptosmobile.com", "to":remail, "subject" : msg, "body" : msg};
		    					var emailOptions = {
		    						url : "https://kryptos.kryptosmobile.com/gateway/CEAI/kryptosemailsender",
		    						headers : {
		    							'licenseKey' : "UjpIaWVDVzpTOjE0NDk0ODk2MzUxMzE6VTpha0BjYW1wdXNlYWkub3JnOlQ6NDpQOjI4Ng=="
		    						},
		    						body : JSON.stringify(emaildata)
		    					}
		    					extrequest.post(emailOptions, function(err, eresp, ebody) {
									otprecord.emailresp = ebody;
									res.json(otprecord);
		    					});	
			    				/*var url = smsurl + "&dest_mobileno=" + otprecord.phone + "&message=" + OTP + " is your One-Time Password (OTP) for " + tenant;
			    				extrequest(url, function(err, resp, body) {
			    					console.log("SMS Resp : " + body);	
			    					res.json(otprecord);
			    				});*/
			  				});
			 			} else {
			 				console.log("result.length " + result.length);
			 				var record = result[0];
			 				var id = new ObjectId(record._id);
			 				var OTP = Math.floor(Math.random()*90000) + 10000;
			 				record.otp = OTP;
			 				console.log(record);
			 				delete record._id;
			 				db.collection(tableName).replaceOne({"_id": id}, record, function(err, result2){
								console.log("Updating the record with id "+id);
								var msg = OTP + " is your One-Time Password (OTP) for " + tenant;
		    					var emaildata = {"from": "donotreply@kryptosmobile.com", "to":remail, "subject" : msg, "body" : msg};
		    					var emailOptions = {
		    						url : "https://kryptos.kryptosmobile.com/gateway/CEAI/kryptosemailsender",
		    						headers : {
		    							'licenseKey' : "UjpIaWVDVzpTOjE0NDk0ODk2MzUxMzE6VTpha0BjYW1wdXNlYWkub3JnOlQ6NDpQOjI4Ng=="
		    						},
		    						body : JSON.stringify(emaildata)
		    					}
		    					extrequest.post(emailOptions, function(err, eresp, ebody) {
									record.emailresp = ebody;
									res.json(record);
		    					});	
								/*var url = smsurl + "&dest_mobileno=" + record.phone + "&message=" + OTP + " is your One-Time Password (OTP) for " + tenant;
			    				extrequest(url, function(err, resp, body) {
			    					console.log("SMS Resp : " + body);
			    					res.json(record);
			    				});*/
							});							
						}
					});
				});
			}
			//res.json({"found" : found, "userinfo" : founduser});
		});
		
	}else {
		res.status(401).json({"Error":"Body should contain 'sheetid',  'email', 'tenant'"});
	}
		
}


exports.validateEmailOTP = function(req, res, next) {
	if (req.body && req.body.tenant  && req.body.otp && req.body.email ) {
		var remail = req.body.email;
		var tenant = req.body.tenant;
		var otp = req.body.otp;
		dbUtil.getConnection(function(db){
			var tableName = "T_" + req.body.tenant + "_EMAIL_OTP";
			db.collection(tableName).find({"tenant" : tenant, "email": remail, "otp" : otp}).toArray(function(err, result){
				if (!result || result.length === 0) {
					res.json({"otpvalid" : false});
				}else {
					res.json({"otpvalid" : true});
				}
			});
		});
	}else {
		res.status(401).json({"Error":"Body should contain 'otp',  'email', 'tenant'"});
	}
}