var geolib = require('geolib');
/**
 * 
 *
 * @param  {[type]}   req  [description]
 * @param  {[type]}   res  [description]
 * @param  {Function} next [description]
 * @return {[type]}        [description]
 */
exports.getdistance = function(req, res, next) {
	if (req.body && req.body.flat && req.body.flng && req.body.tlat && req.body.tlng ) {		
		var distance = geolib.getDistance({latitude: req.body.flat, longitude: req.body.flng},
			{latitude: req.body.tlat, longitude: req.body.tlng});
		res.json({"distance" : distance});
	}else {
		res.status(401).json({"Error":"Body should contain 'flat', 'flng', 'tlat', 'tlng'"});
	}
		
}
