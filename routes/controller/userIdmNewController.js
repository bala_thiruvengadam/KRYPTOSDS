var bcrypt = require('bcryptjs');

var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var extrequest = require('request');
var momentTimezone = require('moment-timezone');
var fs = require('fs');
const utils = require('../lib/utilities');
const bluebird = require('bluebird');
const validRoleName = ['Student', 'Faculty', 'Staff', 'Admin'];

var smsurl = "http://203.129.203.243/blank/sms/user/urlsms.php?username=dealmonk&pass=112233&senderid=DLMONK&response=Y";


exports.validateEmail = function (req, res, next) {
	if (req.body && req.body.tenant && req.body.email) {
		var email = req.body.email;
		var tenant = req.body.tenant;
		dbUtil.getConnection(function (db) {
			var tableName = "T_" + req.body.tenant + "_ALL_USERS";
			db.collection(tableName).find({"email": email}).toArray(function (err, result) {
				if (!result || result.length === 0) {
					res.json({"emailexists": false});
				} else {
					res.json({"emailexists": true});
				}
			});
		});
	} else {
		res.status(401).json({"Error": "Body should contain 'email',  'tenant'"});
	}
}

exports.sendOTPToEmail = function (req, res, next) {
	if (req.body && req.body.tenant && req.body.email) {
		var email = req.body.email;
		var tenant = req.body.tenant;
		dbUtil.getConnection(function (db) {
			var tableName = "T_" + req.body.tenant + "_EMAIL_OTP";
			db.collection(tableName).find({"email": email}).toArray(function (err, result) {
				if (!result || result.length === 0) {
					var OTP = Math.floor(Math.random() * 90000) + 10000;
					console.log('OTP : ' + OTP);
					var otprecord = {"email": email, "otp": OTP};
					db.collection(tableName).insertOne(otprecord, function (err, result) {
						console.log("Inserted a document into the " + tableName + " collection.");
						var msg = OTP + " is your One-Time Password (OTP) for " + tenant;
						var emaildata = {
							"from": "donotreply@kryptosmobile.com",
							"to": email,
							"subject": msg,
							"body": msg
						};
						var emailOptions = {
							url: "https://kryptos.kryptosmobile.com/gateway/CEAI/kryptosemailsender",
							headers: {
								'licenseKey': "UjpIaWVDVzpTOjE0NDk0ODk2MzUxMzE6VTpha0BjYW1wdXNlYWkub3JnOlQ6NDpQOjI4Ng=="
							},
							body: JSON.stringify(emaildata)
						}
						extrequest.post(emailOptions, function (err, eresp, ebody) {
							otprecord.emailresp = ebody;
							res.json(otprecord);
						});
					});
				} else {
					console.log("result.length " + result.length);
					var record = result[0];
					var id = new ObjectId(record._id);
					var OTP = Math.floor(Math.random() * 90000) + 10000;
					record.otp = OTP;
					console.log(record);
					delete record._id;
					db.collection(tableName).replaceOne({"_id": id}, record, function (err, result2) {
						console.log("Updating the record with id " + id);
						var msg = OTP + " is your One-Time Password (OTP) for " + tenant;
						var emaildata = {
							"from": "donotreply@kryptosmobile.com",
							"to": email,
							"subject": msg,
							"body": msg
						};
						var emailOptions = {
							url: "https://kryptos.kryptosmobile.com/gateway/CEAI/kryptosemailsender",
							headers: {
								'licenseKey': "UjpIaWVDVzpTOjE0NDk0ODk2MzUxMzE6VTpha0BjYW1wdXNlYWkub3JnOlQ6NDpQOjI4Ng=="
							},
							body: JSON.stringify(emaildata)
						}
						extrequest.post(emailOptions, function (err, eresp, ebody) {
							record.emailresp = ebody;
							res.json(record);
						});
					});
				}
			});
		});
	}

}

exports.validateEmailOTP = function (req, res, next) {
	if (req.body && req.body.tenant && req.body.otp && req.body.email) {
		var email = req.body.email;
		var tenant = req.body.tenant;
		var otp = req.body.otp;
		dbUtil.getConnection(function (db) {
			var tableName = "T_" + req.body.tenant + "_EMAIL_OTP";
			db.collection(tableName).find({"email": email, "otp": otp}).toArray(function (err, result) {
				if (!result || result.length === 0) {
					res.json({"otpvalid": false});
				} else {
					res.json({"otpvalid": true});
				}
			});
		});
	} else {
		res.status(401).json({"Error": "Body should contain 'otp',  'email', 'tenant'"});
	}
}


exports.activateUser = function (req, res, next) {

	if (req.body && req.body.tenant && req.body.otp && req.body.email && req.body.pwd && req.body.cpwd) {
		var email = req.body.email;
		var tenant = req.body.tenant;
		var otp = req.body.otp;
		var pwd = req.body.pwd;
		var cpwd = req.body.cpwd;
		if (pwd != cpwd) {
			res.json({"error": "Password and Confirm password do not match."});
			return;
		}
		dbUtil.getConnection(function (db) {
			var tableName = "T_" + req.body.tenant + "_EMAIL_OTP";
			db.collection(tableName).find({"email": email, "otp": otp}).toArray(function (err, result) {
				if (!result || result.length === 0) {
					res.json({"error": "Invalid OTP"});
				} else {
					var record = result[0];
					var tenantUserTable = "T_" + tenant + "_ALL_USERS";
					db.collection(tenantUserTable).find({"email": email}).toArray(function (err, result2) {
						if (!result2 || result2.length === 0) {
							var userrecord = {"email": email};
							bcrypt.hash(pwd, 8, function (err, hash) {
								userrecord.pwd = hash;
								userrecord.rollno = record.rollno;
								userrecord.firstname = record.firstname;
								userrecord.lastname = record.lastname;
								userrecord.phone = record.phone;
								userrecord.dateTime = momentTimezone().tz("Asia/Kolkata").format();
								db.collection(tenantUserTable).insertOne(userrecord, function (err, result3) {
									res.json({"success": "User created successfully"});
								});
							});
						} else {
							res.json({"error": "User already exists."});
						}
					});
				}
			});
		});
	} else {
		res.status(401).json({"Error": "Body should contain 'otp',  'email', 'tenant', 'pwd', 'cpwd'"});
	}

}


exports.auth = function (req, res, next) {

	if (req.body && req.body.tenant && req.body.username && req.body.password) {
		var tenant = req.body.tenant;
		var username = req.body.username;
		var password = req.body.password;
		var tenantUserTable = "T_" + tenant + "_ALL_USERS";
		dbUtil.getConnection(function (db) {
			db.collection("T_" + tenant + "_ALL_USERS").find({"email": username}).toArray(function (err, result2) {
				if (!result2 || result2.length === 0) {
					res.status(401).json({"error": "Invalid user name or password."});
				} else {
					var userrecord = result2[0];
					bcrypt.compare(password, userrecord.pwd, function (err, cres) {
						console.log("Compare " + cres);
						if (cres) {
							var loggeduser = userrecord;
							var token = userrecord.pwd;
							loggeduser.usertoken = token;
							delete loggeduser.pwd;
							res.json({"success": "Login Successful", "userinfo": loggeduser});
						} else {
							res.status(401).json({"error": "Invalid user name or password."});
						}
					});
				}
			});
		});
	} else {
		res.status(401).json({"Error": "Body should contain 'username',   'tenant', 'password'"});
	}

}

exports.getAllUserDetails = function (req, res) {
	var tenant = req.params.tenant;

	if (!tenant) {
		return res.json({
			success: false,
			message: 'Missing tenant id.'
		})
	}

	try {
		dbUtil.getConnection(function (db) {
			db.collection("T_" + tenant + "_ALL_USERS").find().toArray(function (err, result) {

				if (err)
					return res.json({
						success: false,
						message: 'Unable to get details.'
					});
				var file = __dirname + '/../../public/user_details.xls';

				if (result.length == 0)
					return res.json({
						success: true,
						message: 'No user found for this tenant.'
					});

				var writeStream = fs.createWriteStream(file);

				var report = "S.No." + "\t" + "Roll No" + "\t" + "Name" + "\t" + "Email" + "\t" + "Phone" + '\t' + "Date (MM-DD-YYYY)" + '\t' + "Time (HH:MM:SS)" + "\n";
				for (var i = 0; i < result.length; i++) {
					var data = result[i];
					report += (i + 1) + '\t' + data.rollno + '\t' + data.firstname + ' ' + data.lastname + '\t' + data.email + '\t' + data.phone + '\t';
					if (data.dateTime) {
						report += data.dateTime.split("T")[0] + '\t' + data.dateTime.split("T")[1].split("+")[0];
					}
					report += '\n';
				}
				writeStream.write(report);
				writeStream.close();
				writeStream.on('close', function () {
					res.download(file);
				});
				writeStream.on('error', function () {
					res.json({
						success: false,
						message: 'Unable to get details.'
					});
				});


			});
		});
	} catch (e) {
		return res.json({
			success: false,
			message: 'Unable to get details.'
		})
	}


}

exports.updateuserprofile = function (req, res, next) {

	if (req.body && req.body.tenant && req.body.email && req.body._id && req.body.usertoken) {
		var email = req.body.email;
		var tenant = req.body.tenant;
		if (!(req.body._id.match(/^[0-9a-fA-F]{24}$/))) {
			res.status(401).json({"Error": "Invalid Object Id " + id});
			return;
		}	
		var id = new ObjectId(req.body._id);
		delete req.body._id;
		dbUtil.getConnection(function (db) {
			var tableName = "T_" + req.body.tenant + "_ALL_USERS";
			db.collection(tableName).find({"_id": id, "pwd": req.body.usertoken}).toArray(function (err, result) {
				if (!result || result.length === 0) {
					res.status(401).json({"Error": "No matching record found for update"});
				} else {
					delete req.body.usertoken;
					delete req.body.tenant;
					delete req.body.email;
					db.collection(tableName).updateOne({"_id": id}, {$set: req.body}, function (err, result) {
						console.log("Updating the Client Data record with id " + id);
						//res.json(req.body);
						db.collection(tableName).find({"_id": id}).toArray(function (err, result) {
							var userinfo = result[0];
							userinfo.usertoken = userinfo.pwd;
							delete userinfo.pwd;
							res.json({"success": "Successfully updated the data", "userinfo": userinfo});
						});
					});
				}
			});
		});
	} else {
		res.status(401).json({"Error": "Body should contain   'email', 'tenant', '_id', 'usertoken'"});
	}
}

exports.getuserprofile = function (req, res, next) {

	if (req.body && req.body.tenant && req.body._id && req.body.usertoken) {
		var tenant = req.body.tenant;
		if (!(req.body._id.match(/^[0-9a-fA-F]{24}$/))) {
			res.status(401).json({"Error": "Invalid Object Id " + id});
			return;
		}
		var id = new ObjectId(req.body._id);
		delete req.body._id;
		dbUtil.getConnection(function (db) {
			var tableName = "T_" + req.body.tenant + "_ALL_USERS";
			db.collection(tableName).find({"_id": id, "pwd": req.body.usertoken}).toArray(function (err, result) {
				if (!result || result.length === 0) {
					res.status(401).json({"Error": "No matching record found"});
				} else {
					var userInfo = result[0];
					var token = userInfo.pwd;
					delete userInfo.pwd;
					userInfo.usertoken = token;
					res.json(userInfo);
				}
			});
		});
	} else {
		res.status(401).json({"Error": "Body should contain  'tenant', '_id', 'usertoken'"});
	}

}


exports.getProfile = function (req, res, next) {

	if (req.body && req.body.tenant && req.body.email) {
		var tenant = req.body.tenant;
		dbUtil.getConnection(function (db) {
			var tableName = "T_" + req.body.tenant + "_ALL_USERS";
			db.collection(tableName).find({"email": req.body.email}).toArray(function (err, result) {
				if (!result || result.length === 0) {
					res.status(401).json({"Error": "No matching record found"});
				} else {
					var userInfo = result[0];
					res.json(userInfo);
				}
			});
		});
	} else {
		res.status(401).json({"Error": "Body should contain  'tenant', '_id', 'usertoken'"});
	}

}


exports.forgotpassword = function (req, res, next) {

	if (req.body && req.body.tenant && req.body.username) {
		var tenant = req.body.tenant;
		var username = req.body.username;
		var tenantUserTable = "T_" + tenant + "_ALL_USERS";
		dbUtil.getConnection(function (db) {
			db.collection("T_" + tenant + "_ALL_USERS").find({"email": username}).toArray(function (err, result2) {
				if (!result2 || result2.length === 0) {
					res.status(401).json({"error": "User not found."});
				} else {
					var userrecord = result2[0];

					var tableName = "T_" + req.body.tenant + "_NFP_OTP";
					db.collection(tableName).find({"email": username}).toArray(function (err, result) {
						if (!result || result.length === 0) {
							var OTP = Math.floor(Math.random() * 90000) + 10000;
							var otprecord = {"email": username, "otp": OTP};
							db.collection(tableName).insertOne(otprecord, function (err, result) {
								//console.log("Inserted a document into the " + tableName + " collection.");
								var url = smsurl + "&dest_mobileno=" + userrecord.phone + "&message=" + OTP + " is your One-Time Password (OTP) for " + tenant;
								//console.log('SMS request : ' + url);
								extrequest(url, function (err, resp, body) {
									//console.log("SMS Resp : " + body);

									otprecord.smsresp = body;
									var msg = OTP + " is your One-Time Password (OTP) for " + tenant;
									var emaildata = {
										"from": "donotreply@kryptosmobile.com",
										"to": userrecord.email,
										"subject": msg,
										"body": msg
									};
									var emailOptions = {
										url: "https://kryptos.kryptosmobile.com/gateway/CEAI/kryptosemailsender",
										headers: {
											'licenseKey': "UjpIaWVDVzpTOjE0NDk0ODk2MzUxMzE6VTpha0BjYW1wdXNlYWkub3JnOlQ6NDpQOjI4Ng=="
										},
										body: JSON.stringify(emaildata)
									}

									extrequest.post(emailOptions, function (err, eresp, ebody) {
										//console.log("Email resp : " + ebody);
										//console.log("Email err : " + err);
										//console.log("Email eresp : " + eresp);
										otprecord.emailresp = ebody;
										res.json(otprecord);
									});
								});
							});
						} else {
							//console.log("result.length " + result.length);
							var record = result[0];
							var id = new ObjectId(record._id);
							var OTP = Math.floor(Math.random() * 90000) + 10000;
							record.otp = OTP;
							//console.log(record);
							delete record._id;
							db.collection(tableName).replaceOne({"_id": id}, record, function (err, result2) {
								//console.log("Updating the record with id "+id);
								var url = smsurl + "&dest_mobileno=" + userrecord.phone + "&message=" + OTP + " is your One-Time Password (OTP) for " + tenant;
								//console.log('SMS request : ' + url);

								extrequest(url, function (err, resp, body) {
									//console.log("SMS Resp : " + body);
									record.smsresp = body;
									//res.json(record);
									var msg = OTP + " is your One-Time Password (OTP) for " + tenant;
									var emaildata = {
										"from": "donotreply@kryptosmobile.com",
										"to": userrecord.email,
										"subject": msg,
										"body": msg
									};
									var emailOptions = {
										url: "https://kryptos.kryptosmobile.com/gateway/CEAI/kryptosemailsender",
										headers: {
											'licenseKey': "UjpIaWVDVzpTOjE0NDk0ODk2MzUxMzE6VTpha0BjYW1wdXNlYWkub3JnOlQ6NDpQOjI4Ng=="
										},
										body: JSON.stringify(emaildata)
									}

									extrequest.post(emailOptions, function (err, eresp, ebody) {
										//console.log("Email resp : " + ebody);
										//console.log("Email err : " + err);
										//console.log("Email eresp : " + eresp);
										record.emailresp = ebody;
										res.json(record);
									});
								});
							});
						}
					});

				}
			});
		});
	} else {
		res.status(401).json({"Error": "Body should contain 'username',   'tenant'"});
	}

}


exports.resetpassword = function (req, res, next) {

	if (req.body && req.body.tenant && req.body.otp && req.body.username && req.body.pwd && req.body.cpwd) {
		var username = req.body.username;
		var tenant = req.body.tenant;
		var otp = req.body.otp;
		var pwd = req.body.pwd;
		var cpwd = req.body.cpwd;
		if (pwd != cpwd) {
			res.json({"error": "Password and Confirm password do not match."});
			return;
		}
		dbUtil.getConnection(function (db) {
			var tableName = "T_" + req.body.tenant + "_NFP_OTP";
			db.collection(tableName).find({"email": username, "otp": otp}).toArray(function (err, result) {
				if (!result || result.length === 0) {
					res.json({"error": "Invalid OTP"});
				} else {
					var record = result[0];
					var tenantUserTable = "T_" + tenant + "_ALL_USERS";
					db.collection(tenantUserTable).find({"email": username}).toArray(function (err, result2) {
						if (!result2 || result2.length === 0) {
							res.json({"error": "User not found."});
						} else {
							var userrecord = result2[0];
							bcrypt.hash(pwd, 8, function (err, hash) {
								console.log(userrecord);
								var updata = {"pwd": hash};
								db.collection(tenantUserTable).updateOne({"_id": userrecord._id}, {$set: updata}, function (err, result) {
									res.json({"success": "Successfully updated the password"});
								});
							});
						}
					});
				}
			});
		});
	} else {
		res.status(401).json({"Error": "Body should contain 'otp',  'username', 'tenant', 'pwd', 'cpwd'"});
	}

}

exports.changepassword = function (req, res, next) {
	if (req.body && req.body.tenant && req.body.oldpwd && req.body.username && req.body.pwd && req.body.cpwd) {
		var username = req.body.username;
		var tenant = req.body.tenant;
		var oldpwd = req.body.oldpwd;
		var pwd = req.body.pwd;
		var cpwd = req.body.cpwd;
		var tenantUserTable = "T_" + tenant + "_ALL_USERS";
		dbUtil.getConnection(function (db) {
			db.collection(tenantUserTable).find({"email": username}).toArray(function (err, result2) {
				if (!result2 || result2.length === 0) {
					res.status(401).json({"error": "User not found."});

				} else {
					var userrecord = result2[0];
					bcrypt.compare(oldpwd, userrecord.pwd, function (err, cres) {
						console.log("Compare " + cres);
						if (cres) {
							if (pwd != cpwd) {
								res.json({"error": "Password and Confirm password do not match."});
								return;
							}
							bcrypt.hash(pwd, 8, function (err, hash) {
								//console.log(userrecord);
								var updata = {"pwd": hash};
								db.collection(tenantUserTable).updateOne({"_id": userrecord._id}, {$set: updata}, function (err, result) {
									res.json({"success": "Successfully updated the password"});
								});
							});
						} else {
							res.status(401).json({"error": "Invalid old password."});
						}
					});
				}
			});
		});
	} else {
		res.status(401).json({"Error": "Body should contain 'oldpwd',  'username', 'tenant', 'pwd', 'cpwd'"});
	}
}

exports.getAllUsers = function(req, res, next) {
	try {
		var tenantName = req.tenantName;
		var batchSize = 20;
		var page = Math.abs(req.query.pgNo) || 1;
		var role = (req.query.role || '').toString();
		var email = (req.query.email || '').toString();
		var unique_id = (req.query.unique_id || '').toString();
		var phone = (req.query.phone || '').toString();
		var firstname = (req.query.firstname || '').toString();
		var lastname = (req.query.lastname || '').toString();
		var course = (req.query.course || '').toString();
		const download = Boolean(req.query.download);
		
		var findQuery = {};

		if ( !utils.isVoid(role) ) {
			findQuery.role = new RegExp(role, 'gi');
		}
		if ( !utils.isVoid(email) ) {
			findQuery.email = new RegExp(email, 'gi');
		}
		if ( !utils.isVoid(unique_id) ) {
			findQuery.unique_id = new RegExp(unique_id, 'gi');
		}
		if ( !utils.isVoid(phone) ) {
			findQuery.phone = new RegExp(phone, 'gi');
		}
		if ( !utils.isVoid(firstname) ) {
			findQuery.firstname = new RegExp(firstname, 'gi');
		}
		if ( !utils.isVoid(lastname) ) {
			findQuery.lastname = new RegExp(lastname, 'gi');
		}
		if ( !utils.isVoid(course) ) {
			findQuery.course = new RegExp(course, 'gi');
		}

		var projection = {
			email: 1,
			role: 1,
			unique_id: 1,
			phone: 1,
			course: 1,
			firstname: 1,
			lastname: 1
		};

		if (download) {
			batchSize = 1048576;
			page = 1;

			projection._id = 0;
		}
		dbUtil.getConn()
			.then(function(db) {
				return db.collection(dbUtil.collection.user(tenantName))
					.find(findQuery, projection)
					.skip(batchSize * (page - 1))
					.limit(batchSize)
					.sort({ _id: -1 })
					.toArray();
			})
			.then(function(userDetails)  {return utils.sendData(req, res, userDetails)})
			.catch(function(err){
				return utils.sendError(
					req, res, 'Unable to get data', err)
				});
	} catch (err) {
		return utils.sendError(req, res, 'Unable to get data', err);
	};
};

exports.addMultipleUnregisteredUsers = function(req, res, next) {
	try {
		var tenantName = req.tenantName;
		var usersList = req.body.users;
		var mandateKeys = [];
		var db;

		if (!(usersList instanceof Array))
			return utils.sendWrongInputError(
				req, res, 'No users to add', 'Users key must be a non-empty array'
			);
		if (usersList.length < 1)
			return utils.sendWrongInputError(
				req, res, 'No users to add', 'Users array atleast have 1 user.'
			);

		var failedRecords = [];
		dbUtil.getConn()
			.then(function(dbConn)  {
				db = dbConn;
				var collectionName = dbUtil.collection.tenantConfig(tenantName)
				return db.collection(collectionName).findOne({
					tenant_name: tenantName
				});
			})
			.then(function(userValidationDetails)  {
				if (!utils.isVoid(userValidationDetails)) {
					mandateKeys = userValidationDetails.user_validation_properties || [];
				}
				mandateKeys.push('role');
				var collectionName = dbUtil.collection.user(tenantName);
				return bluebird.mapSeries(usersList, function(userItem, index) {
					var params = {
						mandateKeys: mandateKeys,
						optionalKeys: [
							'email',
							'unique_id',
							'phone',
							'firstname',
							'lastname',
							'course'
						]
					};
					params = utils.getReqParams(params, userItem);
					if (!utils.isVoid(params.missing))
						failedRecords.push({
							record: index,
							reason: 'Missing value for ' + 
								params.missing.toString()
						});
					else if (validRoleName.indexOf(params.mandateData['role']) < 0) {
						failedRecords.push({
							record: index,
							reason: 'Invalid Role'
						});
					} else  {
						var findQuery = params.mandateData;
						var dataToBeInserted = params.mandateData;
						for(var i in params.mandateData)
							dataToBeInserted[i] = params.mandateData[i];
						for(var i in params.optionalData)
							dataToBeInserted[i] = params.optionalData[i];
						// Object.assign({}, params.mandateData, params.optionalData);
						return db.collection(collectionName)
							.findOne(findQuery)
							.then(function(findResult)  {
								if (utils.isVoid(findResult)) {
									dataToBeInserted.isRegistered = false;
									return db.collection(collectionName)
										.insert(dataToBeInserted);			
								} else {
									var updateQuery = {
										$set: dataToBeInserted };
									return db.collection(collectionName).update(
										findQuery, updateQuery); 
								}
							})
					}
				});
			})
			.then(function(result)  {
				return utils.sendData(req, res, { failedRecords:failedRecords });  
			})
			.catch(function(err)  { return utils.sendError(req, res, 'Unable to add data', err)});
	} catch (err) {
		return utils.sendError(req, res, 'Unable to add data', err);
	};
};

exports.deleteMultipleUnregisteredUsers = function(req, res, next) {
	try {
		var tenantName = req.tenantName;
		var usersList = req.body.users;
		var db;

		if (!(usersList instanceof Array))
			return utils.sendWrongInputError(
				req, res, 'No users to delete', 'Users key must be a non-empty array'
			);
		if (usersList.length < 1)
			return utils.sendWrongInputError(
				req, res, 'No users to delete', 'Users array atleast have 1 user.'
			);
		usersList = usersList.filter(function(user)  {return user.match(/^[0-9a-fA-F]{24}$/);});
		if (usersList.length < 1)
			return utils.sendWrongInputError(
				req, res, 'No users to delete', 'Invalid userid'
			);
		usersList = usersList.map(function(user)  {return new ObjectId(user);});
		dbUtil.getConn()
			.then(function(dbConn)  {
				db = dbConn;
				var collectionName = dbUtil.collection.user(tenantName);
				var findQuery = {
					_id: {
						$in: usersList
					}
				};
				return db.collection(collectionName).remove(findQuery);
			})
			.then(function(result) {return utils.sendData(req, res, 'Users deleted.')})
			.catch(function(err)  { return utils.sendError(req, res, 'Unable to delete Users', err)});
	} catch (err) {
		return utils.sendError(req, res, 'Unable to delete Users', err);
	};
};

exports.editUnregisteredUser = function(req, res, next) {
	try {
		var tenantName = req.tenantName;
		var _id = req.body._id;

		if (!(_id || '').match(/^[0-9a-fA-F]{24}$/))
			return utils.sendWrongInputError(
				req, res, 'User not found', 'Invalid _id'
			);
		_id  = new ObjectId(_id);

		var fieldsToBeUpdate = [
			'email',
			'role',
			'unique_id',
			'phone',
			'firstname',
			'lastname',
			'course'
		];

		var updateQuery = {};
		
		fieldsToBeUpdate.forEach(function(ele)  {
			var r = {};
			r[ele] = req.body[ele];
			if ( !utils.isVoid(r[ele]) ) {
				for(var i in r)
					updateQuery[i] = r[i];
			}
		});

		if (utils.isVoid(updateQuery)) {
			return utils.sendWrongInputError(
				req, res, 'Nothing to update', 'No fields to update'
			);
		}

		updateQuery = { $set: updateQuery };

		var db, findQuery;

		var collectionName = dbUtil.collection.user(tenantName);
		dbUtil.getConn()
			.then(function(dbConn)  {
				db = dbConn;
				if (!utils.isVoid(updateQuery.$set.email)) {	
					findQuery = {
						_id: {
							$ne: _id
						},
						email: updateQuery.$set.email
					};
					return db.collection(collectionName).findOne(findQuery);
				}
				return Promise.resolve({});
			})
			.then(function(emailDetails)  {
				if (!utils.isVoid(emailDetails)) {
					return utils.customPromiseReject(
						'Email-id already resgistered with other user',
						'Email-id already exists');
				}
				findQuery = { _id:_id };
				return db.collection(collectionName).update(
						findQuery, updateQuery);
			})
			.then(function(result)  {
				utils.sendData(req, res, 'User updated');
			})
			.catch(function(err)  {
				if (err.isCustomMsg) {
					return utils.sendWrongInputError(
						req, res, err.prettyMsg, err.message);
				}
					return utils.sendError(
						req, res, 'Unable to update user', err);
			});
	} catch (err) {
		return utils.sendError(req, res, 'Unable to update User', err);
	};
};

exports.validateUser = function(req, res, next) {
	try {
		var tenantName = req.tenantName;
		var params = {
			mandateKeys: [
				'email',
				'phone',
				'unique_id'
			]
		};
		params = utils.getReqParams(params, req.body);
		if ( !utils.isVoid(params.missing) )
			return utils.sendWrongInputError(req, res,
				'Missing values for ' + params.missing.toString(),
				'Missing values for ' + params.missing.toString());

		var collectionName = dbUtil.collection.tenantConfig(tenantName);
		var db;
		dbUtil.getConn()
			.then(function(dbConn)  {
				db = dbConn;
				return db.collection(collectionName)
					.findOne({ tenant_name: tenantName });
			})
			.then(function(tenantDetails)  { return utils.isVoid(tenantDetails)
				? [] : tenantDetails.user_validation_properties;})
			.then(function(validationKeys)  {
				if (!utils.isVoid(validationKeys)
					&& validationKeys instanceof Array) {
					var findQuery = {};
					validationKeys.forEach(function(key)  
						{findQuery[key] = params.mandateData[key];});
					findQuery.isRegistered = false;
					return db.collection(dbUtil.collection.user(tenantName))
						.findOne(findQuery);
				} else
					return Promise.resolve(params.mandateData);
			})
			.then(function(userDetails)  {
				if(!utils.isVoid(userDetails)) {
					return utils.sendOTPToMail(
						params.mandateData['email'], tenantName, params.mandateData['phone'])
						.then(function(otpResult)  {
							return utils.sendData(req, res,
								{ userDetails:userDetails });
						});
				} else {
					return utils.sendWrongInputError(req, res, 'Not a valid user', 'Not a valid User');
				}
			})
			.catch(function(err)  {return utils.sendError(req, res, 'Unable to validate user', err)});
	} catch (err) {
		return utils.sendError(req, res, 'Unable to validate user', err);
	};
};

exports.createUser = function(req, res, next) {
	try {
		var tenantName = req.tenantName;
		var params = {
			mandateKeys: [
				'email',
				'phone',
				'unique_id',
				'otp',
				'firstName',
				'lastName',
				'pwd',
				'role'
			],
			optionalKeys:[
				'course'
			]
		};
		params = utils.getReqParams(params, req.body);
		if ( !utils.isVoid(params.missing) )
			return utils.sendWrongInputError(req, res,
				'Missing values for ' +params.missing.toString(),
				'Missing values for ' +params.missing.toString());
		if (validRoleName.indexOf(params.mandateData.role) < 0)
			return utils.sendWrongInputError(
				req, res, 'Invalid Role', 'Invalid role name');
		if (params.mandateData.role === 'Student'
			&&  utils.isVoid(params.optionalData.course))
			return utils.sendWrongInputError(req, res, 'Course is missing', 'course is missing');
				
		const collectionNames = dbUtil.collectionNames(tenantName);
		var db,
			validationKeys,
			findQuery,
			updateQuery,
			queryOptions;
		dbUtil.getConn()
			.then(function(dbConn)  {
				db = dbConn;
				return db.collection(collectionNames.mailOTP).findOne({
					email: params.mandateData.email,
					otp: params.mandateData.otp
				});
			})
			.then(function(mailOtpDetails)  {
				if (utils.isVoid(mailOtpDetails)) {
					return utils.customPromiseReject(
						'OTP not matched', 'OTP not matched');
				}
				return db.collection(collectionNames.tenantConfig)
					.findOne({ tenant_name: tenantName });
			})
			.then(function(tenantDetails) {
				return utils.isVoid(tenantDetails)
					? [] : tenantDetails.user_validation_properties;
			})
			.then(function(result)  {
				validationKeys = result;
				if (validationKeys instanceof Array) {
					findQuery = {};
					validationKeys.forEach(function(key)  
						{
							findQuery[key] = params.mandateData[key]
						});
					if(validationKeys.length > 0) {
						findQuery.isRegistered = false;
						return db.collection(collectionNames.user)
							.findOne(findQuery)
					}
					return Promise.resolve(params.mandateData);
				} else
					return utils.customPromiseReject(
						'Invalid Validation Properties',
						'Invalid tenant validation properties');
			})
			.then(function(userDetails)  {
				if(utils.isVoid(userDetails)) {
					return utils.customPromiseReject(
						validationKeys.toString() + ' not registered',
						validationKeys.toString() + ' not registered');
				} else {
					return utils.getPwdHash(params.mandateData.pwd);
				}
			})
			.then(function(pwdHash)  {
				if (utils.isVoid(pwdHash)) {
					return utils.customPromiseReject(
						'Something went wrong',
						'Password Hash not generated.');
				}
				if (utils.isVoid(findQuery))
					findQuery = {email: params.mandateData.email};

				updateQuery = { $set: {
					isRegistered: true,
					firstname: params.mandateData.firstName,
					lastname: params.mandateData.lastName,
					email: params.mandateData.email,
					phone: params.mandateData.phone,
					unique_id: params.mandateData.unique_id,
					pwd: pwdHash,
					course: params.optionalData.course,
					dateTime: momentTimezone().tz("Asia/Kolkata").format()
				} };
				queryOptions = {
					upsert: true,
					new: true
				};
				return db.collection(collectionNames.user)
					.findAndModify(findQuery, [], updateQuery, queryOptions);
			})
			.then(function(updateResult)  {
				if (updateResult && updateResult.value) {
					updateResult.value.usertoken = updateResult.value.pwd;
					delete updateResult.value.isRegistered;
					delete updateResult.value.pwd;
					return utils.sendData(req, res, {
						userinfo: updateResult.value
					});
				} else {
					return utils.sendWrongInputError(
						req, res, 'Unable to create user.', 
						'Unable to modify user'
					);
				}
			})
			.catch(function(err)  {
				if (err.isCustomMsg) {
					return utils.sendWrongInputError(
						req, res, err.prettyMsg, err.message);
				}
					return utils.sendError(
						req, res, 'Unable to create user', err);
			});
	} catch (err) {
		return utils.sendError(req, res, 'Unable to create user', err);
	};
};

exports.updateValidationProperties = function(req, res, next) {
	try {
		var allowedValidationProperties = ['email', 'phone', 'unique_id'];
		var tenantName = req.tenantName;
		var validationProperties = req.body.validationProperties;
		var acceptedValidationProperties = [];
		if (!(validationProperties instanceof Array)) {
			return utils.sendWrongInputError(req, res,
				'Invalid validation properties',
				'validationProperties must be an Array')
		}
		validationProperties.forEach(function(v, i) {
			var index = allowedValidationProperties.indexOf(v);
			if ( index > -1) {
				acceptedValidationProperties.push(v);
				allowedValidationProperties.splice(index, 1);
			}
		});
		var findQuery = {
			tenant_name: tenantName
		};
		var updateQuery = {
			$set: {
				tenant_name: tenantName,
				user_validation_properties: acceptedValidationProperties
			}
		};
		var queryOptions = {
			new: true,
			upsert: true
		};
		var collectionName = dbUtil.collection.tenantConfig(tenantName);
		dbUtil.getConn()
			.then(function(db) {
				return db.collection(collectionName).update(
					findQuery, updateQuery, queryOptions);
			})
			.then(function(updateResult)  {
				return utils.sendData(req, res, {
					message: 'Updated with ' +
						(acceptedValidationProperties.length > 0
						? acceptedValidationProperties.concat()
						: '0 validations')
				});
			})
			.catch(function(err)  {
				return utils.sendError(req, res,
					'Unable to update data', err);
			});
	} catch (err) {
		return utils.sendError(req, res, 'Unable to update data', err);
	};
};
exports.getValidationProperties = function(req, res, next) {
	try {
		var tenantName = req.tenantName;
		var collectionName = dbUtil.collection.tenantConfig(tenantName);
		dbUtil.getConn()
			.then(function(db) {
				return db.collection(collectionName)
				.findOne({
					tenant_name: tenantName
				});
			})
			.then(function(validKeys)  {
				var validationKeys = [];
				if (!utils.isVoid(validKeys)) {
					validationKeys = validKeys.user_validation_properties || [];
				}
				return utils.sendData(
					req, res, {
						validationKeys:validationKeys
					});
			})
			.catch(function(err)  {
				return utils.sendError(
					req, res, 'Unable to get Validation Properties', err);
			});
	} catch (err) {
		return utils.sendError(req, res, 'Unable to Validation Properties', err);
	};
};