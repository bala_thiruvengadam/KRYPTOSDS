'use-strict'

/*-Module import*/
var crypto = require('crypto');
var ObjectId = require('mongodb').ObjectID;
/*Global Vars*/
const merchant_id = "IMADELM";
const encrypt_key = 'anjueolkdiwpoida';
var dbUtil = require("../../config/dbUtil");

exports.getOperatorKeywords = function(req, res) {
    dbUtil.getConnection(function(db) {
        db.collection("operator_keyword").find({}).toArray(function(err, opKeys) {
            if (err) {
                res.status(501).send("Something went wrong");
            } else {
                res.json(opKeys);
            }
        });
    });
};
exports.getRegionAliasMapping = function(req, res) {
    dbUtil.getConnection(function(db) {
        db.collection("region_alias").find({}).toArray(function(err, regionAlias) {
            if (err) {
                res.status(501).send("Something went wrong");
            } else {
                res.json(regionAlias);
            }
        });
    });
};

exports.saveloadmoneytransaction = function(req, res) {


    var oxitrxid = req.body.oxitrxid;
    var mtrxid = req.body.mtrxid;
    var trxstatus = req.body.trxstatus;
    var trxmsg = req.body.trxmsg;
    var paidamt = req.body.paidamt;
    var OtherVals = req.body.OtherVals || {};

    try {
        dbUtil.getConnection(function(db) {
            var tableName = "T_OXIGEN_LOAD_MONEY_RESPONSE";
            var data = {
                oxitrxid: oxitrxid,
                mtrxid: decrypt(mtrxid),
                trxstatus: decrypt(trxstatus),
                trxmsg: decrypt(trxmsg),
                paidamt: decrypt(paidamt),
                OtherVals: OtherVals
            };
            db.collection(tableName).insertOne(data, function(err, result) {
                if (err)
                    res.json({
                        success: false,
                        message: err.message
                    });
                else
                    res.json({
                        success: true,
                        message: 'Record saved successfully.'
                    });
            });
        });

    } catch (e) {
        res.json({
            success: false,
            message: 'Unable to save record.'
        });
    }
};

function decrypt(ciphertext) {

    try {
        var decipher = crypto.createDecipheriv('aes-128-cbc', encrypt_key, encrypt_key);
        decipher.setAutoPadding(true);
        var decrypted = decipher.update(ciphertext, 'base64', 'binary');
        try {
            decrypted += decipher.final('binary');
        } catch (e) {
            console.log(e);
        }

        return decrypted.toString('utf8');
    } catch (e) {
        return ciphertext;
    }

}

exports.getloadmoneytransaction = function(req, res) {

    var mtrxid = req.params.mtrxid;

    if (!mtrxid) {
        return res.send({
            success: false,
            message: 'Missing transaction id.'
        });
    }

    try {
        dbUtil.getConnection(function(db) {
            var tableName = "T_OXIGEN_LOAD_MONEY_RESPONSE";
            db.collection(tableName).find({
                    mtrxid: mtrxid
                })
                .toArray(function(err, result) {
                    if (err)
                        res.json({
                            success: false
                        });
                    else
                        res.json({
                            success: true,
                            data: result[0]
                        })
                });
        });

    } catch (e) {

        console.log(e);

    }


}