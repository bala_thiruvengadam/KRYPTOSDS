var bcrypt = require('bcryptjs');
var geolib = require('geolib');
var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var extrequest = require('request');

exports.create = function(req, res, next) {
    if (req.body && req.body.campaign && req.body.title && req.body.code && req.body.startdate && req.body.enddate && req.body.url  && req.body.image) {
        
        var campaign = req.body.campaign;
        var title = req.body.title;
        var code = req.body.code;
        var startdate = req.body.startdate;
        var enddate = req.body.enddate;
        var url = req.body.url;
        var image = req.body.image;
       
        try {
            dbUtil.getConnection(function(db) {
            var tableName = "T_COUPONS";
            db.collection(tableName).find({
                    "code": code
                }).toArray(function(err, result) {
                    if(result.length > 0){
                        res.json({"Error":"coupon code already exist."})
                    }else{
                        dbUtil.getConnection(function(db) {
                        
                        var data = {
                            'campaign': campaign,
                            'title': title,
                            'code': code,
                            'startdate': startdate,
                            'enddate': enddate,
                            'url': url,
                            'image':image
                        };
                        db.collection(tableName).insertOne(data, function(err, result3) {
                            res.json({
                                "success": "coupon added successfully"
                                });
                            });
                        }); 
                    }
                });
            });
            
        } catch (e) {
            console.log(e)
        }
    } else {
            res.status(401).json({"Error":"Body should contain 'campaign','title','code','enddate','startdate','url,'image'"});
    }
}

exports.getCoupons = function(req, res, next) {
    dbUtil.getConnection(function(db) {
                var tableName = "T_COUPONS";
                db.collection(tableName).find({
                }).toArray(function(err, result) {
                    res.json(result);
                });
            });
}