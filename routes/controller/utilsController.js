var base64Img = require('base64-img');
/** Modules import */
var request = require('request');

exports.encode = function (req, res, next) {
    if (req.body && req.body.url) {
        var url = req.body.url;
        base64Img.requestBase64(url, function (err, resp, body) {
            //console.log(resp);
            //console.log(body);
            res.json({"data": body});
        });
    } else {
        res.status(401).json({"Error": "Body should contain 'url'"});
    }

};

exports.proxyCall = function (req, res) {

    var opt = req.body;

    request(opt, function (error, response, body) {
        console.log(error);
        console.log(response);
        console.log(body);
        res.send(body);
    });


}